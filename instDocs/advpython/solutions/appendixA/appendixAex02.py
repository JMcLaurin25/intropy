#!/usr/bin/env python3
#
# A Solution For AppendixA Exercise 2
#
import zlib, sys

f = open(sys.argv[1], "rb")
data = f.read()
f.close()

compressed = zlib.compress(data)

f = open("output.zip", "wb")
f.write(compressed)
f.close()

# Read contents of file back in to confirm
# that eveeryting worked
f = open("output.zip", "rb")
data = f.read()
f.close()
print("\nOriginal Contents:\n")
print(zlib.decompress(data).decode())
