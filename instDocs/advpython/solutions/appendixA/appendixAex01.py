#!/usr/bin/env python3
#
# A Solution For AppendixA Exercise 1
#
class NumberRange:
    """Iterator: loops backwards over seq """
    def __init__(self, begin, end):
        self.number_range = range(min(begin, end), max(begin,end) + 1)
        self.index = -1

    def __iter__(self):
        return self

    def __next__(self):
        if self.index == len(self.number_range) -1 :
            raise StopIteration
        self.index += 1
        return self.number_range[self.index]

#automatic calls to __next__
r = NumberRange(4,9)
for char in r:
    print(char)

# manual calls to __next__
r = NumberRange(13,7)
print(next(r), next(r), next(r), next(r))
print(next(r), next(r), next(r))
print("*" * 30)
#This will generate a StopIteration error
next(r)
