#!/usr/bin/env python3
#
# A Solution For Chapter 2 Exercise 1
#
def whattype(p):
        for item in p:
                if ( isinstance(item, int) ):
                        print("Int", end="")
                elif (isinstance(item, list) ):
                        print("List", end="")
                elif (isinstance(item, dict) ):
                        print("Dict", end="")
                elif (isinstance(item, str) ):
                        print("str", end="")
                print("\t", item)

data = [2,3,4,5]
x = 10
h = {}
h["Michael"] = "Mike"
h["Judy"] = "Judith"

whattype([data, x, h, "Hello"])
        
