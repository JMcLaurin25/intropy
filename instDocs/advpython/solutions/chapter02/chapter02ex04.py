#!/usr/bin/env python3
#
# A Solution For Chapter 2 Exercise 4
#
import math
fact = [ (x, math.factorial(x)) for x in range(5,8) ]
print(fact)
