#!/usr/bin/env python3
import cgi
print("Content-type: text/html\n\n")
form = cgi.FieldStorage()
print("<HTML><BODY BGCOLOR=Yellow>")
fname = form.getfirst("firstname")
lname = form.getfirst("lastname")
address = form.getfirst("address")
phone = form.getfirst("phone")
toppings = form.getlist("toppings")
size = form.getfirst("size")
print("<center>YOUR ORDER INFORMATION</center><BR>")
name = fname + " " + lname
print("<center><table border=5><tr><td>NAME</td><td>"+name+"</td></tr>")
print("<tr><td>ADDRESS</td><td>"+address+ "</td></tr>")
print("<tr><td>PHONE</td><td>" + phone + "</td></tr>")

print("<tr><td>TOPPINGS</td><td>" + str(toppings) + "</td></tr>")
price = 9.00
if ( size == 'Medium' ):
	price = 12.00
elif ( size == 'Large'):
	price = 15.00
tops = len(toppings) * 0.50
price = price + tops
print("<tr><td>SIZE</td><td>" + size  + "</td></tr>")
print("<tr><td>PRICE</td><td>" + str(price)  + "</td></tr>")
print("</table></BODY></HTML>")
