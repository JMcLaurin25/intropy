#!/usr/bin/env python3
#
# A Solution For Chapter 1 Exercise 2
#
class SortedArray:
	def __init__(self):
		self.data = []
	def add(self, item):
		self.data.append(item)
		self.data.sort()
	def __str__(self):
		return str(self.data)
	def shift(self):
		return self.data.pop(0)
	def __len__(self):
		return len(self.data)
	def pop(self):
		return self.data.pop()
