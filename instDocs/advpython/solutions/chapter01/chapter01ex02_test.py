#!/usr/bin/env python3
#
# A Test For The Solution For Chapter 1 Exercise 2
#
from chapter01ex02 import SortedArray

s = SortedArray()
s.add(4)
s.add(10)
s.add(7)
print(s)
x = s.shift()
print("removed: " + str(x))
print(s)
print(len(s))
y = s.pop()
print("removed: " + str(y))
print(s)
