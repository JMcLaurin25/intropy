#!/usr/bin/env python3
#
# A Solution For Chapter 5 Exercise 1
#
import os
input = os.popen("ls", "r")
lines = input.readlines()
lines.sort(key=len)
for file in lines:
	print(file, end="")
