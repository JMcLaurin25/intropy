#!/usr/bin/env python3
#
# A Solution For Chapter 5 Exercise 1
#
import os, time
def sorter(p):
	return hash[p]
hash = {}
input = os.popen("ls", "r")
lines = input.readlines()
for file in lines:
	file = file[0:-1]
	info = os.stat(file)
	hash[file] = info[8]
keys = list(hash.keys())
keys.sort(key=sorter)
for key in keys:
	print("%-15s %30s" %  (key, time.ctime(hash[key])))
