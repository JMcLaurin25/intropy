#!/usr/bin/env python3
#
# A Solution For Chapter 5 Exercise 1
#
import sys, os
for command in sys.argv[1:]:
	pid = os.fork()
	if pid == 0:
		os.execv("/bin/" + command, [command])
		printf("error in forking ")
		exit()
	elif pid > 0:
		os.wait()
	else:
		print("error in forking\n");
