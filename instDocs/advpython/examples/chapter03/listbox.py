#!/usr/bin/env python3
from tkinter import *
import os

def getselected():
    item = listbox.curselection()
    li = list(item)
    if  not li:
        print("No selection!")
        return
    for i in li:
        print(listbox.get(i))

master = Tk()
Label(master, text="Select from List").pack()
listbox = Listbox(master, selectmode=MULTIPLE)
listbox.pack()
Button(master, text="Get Selection",
       command=getselected).pack()
for item in os.listdir("."):
    listbox.insert(END, item)
master.mainloop()
