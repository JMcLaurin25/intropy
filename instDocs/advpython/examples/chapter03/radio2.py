#!/usr/bin/env python3
from tkinter import *
import calendar

def listthem():
    print(var.get())
	
master = Tk()
master.title("Days of the Week")
Label(master, text="Select A Day").pack()
days_long = list(calendar.day_name)
days_abbr = list(calendar.day_abbr)
var = StringVar()
for day_long, day_abbr in zip(days_long, days_abbr):
    radio = Radiobutton(master, value=day_long,
                text = day_abbr, variable = var,
                command=listthem)
    radio.pack(side=LEFT)
master.mainloop()
