#!/usr/bin/env python3
from tkinter import *
from tkinter.messagebox import *
def go():
    print("go")
def callback():
    if askyesno('Verify', 'Really Quit'):
        showinfo("Quiting",
                 "Quit has been\nselected")
        exit(0)
root = Tk()
root.geometry("150x50")
b1 = Button(root, text="quit", command=callback)
b1.pack(side=LEFT)
b2 = Button(root, text="keep going", command=go)
b2.pack(side=LEFT)
root.mainloop()
