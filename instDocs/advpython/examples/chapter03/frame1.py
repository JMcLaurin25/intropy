#!/usr/bin/env python3
from tkinter import *

root = Tk()
root.geometry("300x300")
root.title("Three Frames")
frame1 = Frame(root)
Label(frame1, text="frame 1").pack()
frame1.pack(expand = 1, fill=BOTH)

frame2 = Frame(root, bg="#00ffff")
Label(frame2, text="frame 2").pack()
frame2.pack(expand=1, fill=BOTH)

frame3 = Frame(root, bg="#ffff00")
Label(frame3, text="frame 3").pack()
frame3.pack(expand=1, fill=BOTH)

root.mainloop()
