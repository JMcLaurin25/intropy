#!/usr/bin/env python3
from tkinter import *
from tkinter.messagebox import *
def cls():
    t.delete('1.0','end')
def show():
    filename = e.get()
    try:
        f = open(filename, "r")
        lines = f.readlines()
        for line in lines:
            t.insert('end', line)
    except OSError:
        showerror('ERROR', 'No file ' + filename)

Label(text="Enter File Name").pack()
e = Entry(width=20)
e.pack()
t = Text(height=10,width=70)
t.pack()
Button(text="Show File", command=show).pack()
Button(text="Clear Text", command=cls).pack()
mainloop()
