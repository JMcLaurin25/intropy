#!/usr/bin/env python3
from tkinter import *

def listthem():
    if not var1.get():
        print("Nothing Selected")
        return
    print(txt[var1.get()])
	
master = Tk()
var1 = IntVar()
Label(master, text="Select A Favorite Sport").pack()
b1 = Button(master, text="List Selected",
            command=listthem)
b1.pack(side=TOP)
txt = ["Basketball", "Football", "Baseball"]
radios = [Radiobutton(master, value=0),
          Radiobutton(master, value=1),
          Radiobutton(master, value=2)]
for radio, s in zip(radios, txt):
    radio["text"] = s
    radio["variable"] = var1
    radio.pack(side=LEFT)
master.mainloop()
