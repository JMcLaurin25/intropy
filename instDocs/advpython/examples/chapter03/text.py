#!/usr/bin/env python3
from tkinter import *

def grabtxt():
    line = ent01.get()
    pos = ent02.get()
    data = txt.get(line + "." + pos, END)
    print(data)
def deltxt():
    txt.delete(line + "." + pos, 'end')

root = Tk()
Label(root, text="Starting Line Number:").pack()
ent01 = Entry(root)
ent01.pack()
Label(root, text="Starting pos within line").pack()
ent02 = Entry(root)
ent02.pack()
b1 = Button(root, text="Grab Text", command=grabtxt)
b1.pack()
b2 = Button(root, text="DeleteText", command=deltxt)
b2.pack()
b3 = Button(root, text="Quit", command=root.destroy)
b3.pack()
txt = Text(root, height=8, width=50)
txt.insert("end", "This is line 1\nThis is line 2")
txt.pack()

root.mainloop()
