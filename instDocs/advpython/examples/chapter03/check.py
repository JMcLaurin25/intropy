#!/usr/bin/env python3
from tkinter import *

def listthem():
    state = [v1.get(), v2.get(), v3.get()]
    print("Checkbox states:", state)
    if not sum(state):
        print("Nothing Selected!")
        return
    if state[0]:
        print(c1["text"])
        c1.deselect()
    if state[1]:
        print(c2["text"])
        c2.deselect()
    if state[2]:
        print(c3["text"])
        c3.deselect()

master = Tk()

v1, v2, v3 = IntVar(), IntVar(), IntVar()
Label(master, text="Select Favorite Sports").pack()
b1 = Button(master, text="List Selected",
            command=listthem)
b1.pack(side=TOP)

c1 = Checkbutton(master, text="Basketball",
                 variable=v1)
c2 = Checkbutton(master, text="Football",
                 variable=v2)
c3 = Checkbutton(master)
c3["text"] = "Baseball"   # setting properties as
c3["variable"] = v3       # key/value pairs can be
                          # done for any Widget
c1.pack(side=LEFT)
c2.pack(side=LEFT)
c3.pack(side=LEFT)
master.mainloop()
