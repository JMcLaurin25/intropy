#!/usr/bin/env python3
import sys, socket

host = '0.0.0.0'  # default server address
port = 2345       # deafult server port
buffersize = 1024
if len(sys.argv) == 2:
    host = sys.argv[1]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))
prompt = "Enter a Python Expression (or 'quit'): "
while True:
    line = input(prompt)
    if line == "quit":
        break
    s.send(line.encode())
    data = s.recv(buffersize)
    print('Result: ', data.decode())
s.close()
