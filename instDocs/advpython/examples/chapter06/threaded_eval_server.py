#!/usr/bin/env python3
from socket import *
from handler_function import handle_client
import threading

class ClientHandler(threading.Thread):
    def __init__(self, csocket):
        threading.Thread.__init__(self)
        self.csocket = csocket

    def run(self):
        client = self.csocket
        while True:
            data = client.recv(1024)
            if not data:
                break
            try:
                result = str(eval(data.decode()))
                client.send(result.encode())
            except:
                client.send(b'Invalid Expression')
        client.shutdown(SHUT_RDWR)
        client.close()

host = '0.0.0.0'  # server address
port = 2345       # server port

s = socket(AF_INET, SOCK_STREAM)
s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
s.bind((host,port))
s.listen(5)  # max number of queued connections

while True:
    try:
        print("Waiting for connection...")
        client, client_addr = s.accept()
        print("Connection from ...", client_addr)
        client_thread = ClientHandler(client)
        client_thread.start()
    except KeyboardInterrupt:
        break;
    except Exception as err:
        print(err)

print("\nShutting down the server")
s.shutdown(SHUT_RDWR)
s.close()
