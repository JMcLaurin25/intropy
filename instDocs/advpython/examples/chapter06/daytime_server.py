#!/usr/bin/env python3
import sys, socket, time
from socket import *
host = 'localhost'  # server address
port = 2013         # server port

s = socket(AF_INET, SOCK_STREAM)
s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
s.bind((host,port))
s.listen(5)  # max number of queued connections

while True:
    try:
        print("Waiting for connection...")
        client, client_addr = s.accept()
        print("Connection from ...", client_addr)
        thetime = time.ctime(time.time())
        client.send(bytes(thetime, 'utf-8'))
        client.shutdown(SHUT_RDWR)
        client.close()
    except KeyboardInterrupt:
        break;
    except Exception as err:
        print(err)

print("\nShutting down the server")
s.shutdown(SHUT_RDWR)
s.close()
