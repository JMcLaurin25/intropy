#!/usr/bin/env python3
import sys, socket

host = 'time.nist.gov'  # default server address
port = 13               # deafult server port
buffersize = 1024
if len(sys.argv) == 3:
    host = sys.argv[1]
    port = int(sys.argv[2])

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))
data = s.recv(buffersize)
print(type(data), data)
print('Received:', data.decode()) # Decode to string
s.close()
