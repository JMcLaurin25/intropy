#!/usr/bin/env python3
import pickle
grades = { 'mike': {'exam01':93,
                    'exam02':79,
                    'exam03':99 },
            'sue': {'exam01':95,
                    'exam02':93,
                    'exam03':98}}

file = open('grades', 'wb')
pickle.dump(grades, file)
file.close()

file = open('grades', 'rb')
data = pickle.load(file)
print(data)
file.close()

print(data is grades)
print(data == grades)
