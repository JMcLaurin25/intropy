#!/usr/bin/env python3
import sqlite3
conn = sqlite3.connect('dbase1')
print("connected to database")
curs = conn.cursor()
cmd = 'create table people \
       (name char(30), job char(10), pay int(4))'
curs.execute(cmd)
rows = [['mike', 'dev', '50000'],
        ['joan', 'mus', '70000'],
        ['mike', 'adm', '40000'],
        ['peter', 'adm', '40000'],
        ['larry', 'dev', '55000'],
        ['moe', 'mus', '60000'],
        ['curley', 'adm', '45000']]

while True:
    line = input("enter a name, job, and pay ")
    if not line:
        break
    (n,j,p) = line.split()
    curs.execute('insert into people values(?,?,?)',
                  [n,j,p])
for row in rows:
    curs.execute('insert into people values(?,?,?)',
                 row)
conn.commit()
curs.execute('select * from people')

for n,o,p in curs.fetchall():
    print(n, o, p)
curs.execute('select * from people')
for n,o,p in curs.fetchall():
    print(n, p)
