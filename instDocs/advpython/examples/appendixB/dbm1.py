#!/usr/bin/env python3
import dbm
file = dbm.open('history', 'c')

file['1812'] = 'Revolutionary War'
file['1860'] = 'Civil War'
file['1898'] = 'Spanish/Am War'

print(file.keys())

print(file['1898'])
print(file['1898'].decode())

print(len(file))
print(b'1898' in file)
file.close()
