#!/usr/bin/env python3
import os
print("  UID = ", os.getuid())
print("  GID = ", os.getgid())
print("  PID = ", os.getpid())
print(" PPID = ", os.getppid())
print("LOGIN = ", os.getlogin())
print("  PWD = ", os.getcwd())
