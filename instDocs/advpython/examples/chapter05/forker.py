#!/usr/bin/env python3
import os, sys
app_pid = os.getpid()
print("Application PID is :", app_pid)
print("Forking will create a second process...\n")
pid_from_fork = os.fork()

# The following code will now be executed by both
# the parent & child process - in no particular order
if ( pid_from_fork > 0 ):
    print("Parent Proces is running",app_pid,
          pid_from_fork, os.getpid())
    os.wait()
    print("Parent Process returned from wait()")
elif ( pid_from_fork == 0 ):
    print("Child Proces is running",app_pid,
          pid_from_fork, os.getpid())
else:
    print("error in forking!")

print("Both processes will print this", os.getpid())
