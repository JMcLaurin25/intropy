#!/usr/bin/env python3
import os, sys
pid_from_fork = os.fork()

if ( pid_from_fork > 0 ):
    print("Parent Proces will wait until")
    print("Child Process has completed\n")
    os.wait()
elif ( pid_from_fork == 0 ):
    alist = ["echo", "show", "this", "text"]
    os.execv("/bin/echo", alist)
else:
    print("error in forking!")

print("*" * 30)
pid_from_fork = os.fork()

if ( pid_from_fork > 0 ):
    print("Parent Proces will wait until")
    print("Child Process has completed\n")
    os.wait()
elif ( pid_from_fork == 0 ):
    alist = ["echo", "show", "this", "text"]
    os.execvp("echo", alist)
else:
    print("error in forking!")
