#!/usr/bin/env python3
import os, sys, time
for file in sys.argv[1:]:
	if ( os.path.isfile(file)) :
		info = os.stat(file)
		print(file)
		print("\t", time.ctime(info.st_atime),
              "Last Access Time")
		print("\t", time.ctime(info.st_mtime),
              "Last Mod Time")
		print("\t", time.ctime(info.st_ctime),
              "Last Inode Change")
	else:
		print(file, "is not a filename");
