#!/usr/bin/env python3
def get_customers():
    thefile = open("customers.txt", "r")
    customer_list = thefile.readlines();
    thefile.close()
    # use list comprehension to convert to a
    # nested list of lists (each a list of strings)
    return [customer.rstrip().split(",") for
                     customer in customer_list]

def get_info(nestedlist):
    info = str(type(nestedlist)) + "\n"
    info += str(type(nestedlist[0])) + "\n"
    info += str(type(nestedlist[0][0])) + "\n\n"
    # partial contents of nestedlist
    for i in range(3):
        info += str(nestedlist[i]) + "\n"
    return info

def get_dictionary(nestedlist):
    # Convert nested list to a dictionary
    # using a dictionary comprehension
    return {cust[0] + " " + cust[1] : cust
                 for cust in nestedlist}

def user_interaction(customers):
    tags = ["FirstName", "LastName", "Street",
            "City","State", "Zipcode"]
    while True:
        name = input("Enter a customer's name:")
        if name == "quit": break
        data = customers[name]
        for i, value in enumerate(data):
            print(tags[i], ": ", value)

def print_customernames(customer_map):
    print("Here are the customers names:")
    for i, customer in enumerate(customer_map):
        print("{0:18}".format(customer), end = "")
        if i % 4 == 3: print()
    print("\n")
