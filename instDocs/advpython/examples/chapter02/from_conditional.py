#!/usr/bin/env python3
words = ["hello","racecar","eye", "bike", "stats",
         "civic"]
palindromes = [x for x in words if x[len(x)::-1]== x]
print(palindromes)
