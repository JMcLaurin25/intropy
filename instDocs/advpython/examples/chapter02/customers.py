#!/usr/bin/env python3
from customer_functions import *

customers = get_customers()
print(get_info(customers))
customer_map = get_dictionary(customers)
print_customernames(customer_map)
user_interaction(customer_map)
