#!/usr/bin/env python3
from statecaps import *
# Sorting dictionary by keys
states = list(statecaps.keys())
states.sort()
for state in states:
    print(state, ":", statecaps[state])

print("*" * 80)

def customsort(akey):
    return statecaps[akey]

# Sorting dictionary by keys
states = list(statecaps.keys())
states.sort(key=customsort)
for state in states:
    print(statecaps[state], ":", state)
