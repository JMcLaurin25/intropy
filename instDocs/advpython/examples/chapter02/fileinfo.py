#!/usr/bin/env python3
import os
files = os.listdir(".")
fileinfo = {f : os.path.getsize(f) for f in files}
for name, size in fileinfo.items():
    print("{0:24} : {1:>6,} bytes".format(name,size))
