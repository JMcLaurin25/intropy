#!/usr/bin/env python3
words = ["hello","racecar","eye", "bike", "stats",
         "civic"]
pairs = [[y.upper(), len(y)] for y in 
          [x for x in words if x[len(x)::-1]== x]]
print(pairs)
