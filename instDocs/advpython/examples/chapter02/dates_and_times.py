#!/usr/bin/env python3
import time, datetime, calendar

# time module
start_time = time.perf_counter()
time.sleep(1)
now = time.time()
print(now)
print(time.ctime(now))
t = time.localtime(now)
f = "{0}/{1}/{2}"
print(f.format(t.tm_mon,t.tm_mday,t.tm_year))
print(f.format(t[1],t[2],t[0]))
print(time.strftime("%m/%d/%Y", t))

# datetime module
adate = datetime.date.today()
print(adate)
print(f.format(adate.month, adate.day, adate.year))

# calendar module
print(list(calendar.day_name))
print(list(calendar.day_abbr))
print(list(calendar.month_name))
print(list(calendar.month_abbr))
end_time = time.perf_counter()
print("Running time:", end_time - start_time)
