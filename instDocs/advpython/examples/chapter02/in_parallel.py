#!/usr/bin/env python3
long_names = ["January", "February", "March",
              "April", "May", "June", "July",
              "August", "September", "October",
              "November", "December"]
abbr_names = ["Jan", "Feb", "Mar", "Apr", "May",
              "Jun", "Jul","Aug", "Sep", "Oct",
              "Nov", "Dec"]
numdays = [31, 28, 31, 30, 31, 30, 31, 31, 30,
           31, 30, 31]
for a, b, c in zip(numdays, abbr_names, long_names):
    print("# of Days:", a, end="    ")
    print("Abbr Name:", b, end="    ")
    print("Long Name:", c)
