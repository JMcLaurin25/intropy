#!/usr/bin/env python3
import cgi
print("Content-type: text/html\n")
fields = cgi.FieldStorage()
print("<html><head><title>CGI 101</title></head>")
amt =  1
rate = years = 0
if "years" in fields:
    years = fields['years'].value
if "amount"  in fields:
    amt = fields['amount'].value
if "rate" in fields:
    rate = fields['rate'].value
print("<center><h4>Growth Table</center>")
print("<center><h4>$" + amt + " compounded for ")
print(years  + " years at ")
print(float(rate) * 100)
print("%: </center><body><center><TABLE BORDER=4>")
print("<th>YEAR<td>VALUE</td><td></td></th>")
for yrs in range(int(years) ):
    print("<TR><TD>")
    print(yrs + 1)
    print("</TD>")
    print("<TD>")
    print(float(amt))
    print("</TD></TR>")
    amt = float(amt) * float(rate) + float(amt)
print("</TABLE></center></body></html>")
