#!/usr/bin/env python3
import cgi
print("Content-type: text/html\n")
fields = cgi.FieldStorage()
print("<html><head><title>CGI 101</title></head>")
print("<body")
print("<h1>A First CGI Example</h1>")
print("<P>Hello, CGI World!</p>")
x=""
if "firstname" in fields:
    x = fields['firstname'].value
    print(x)
if "lastname" in fields:
    x = fields['lastname'].value
    print(x)
if "course" in fields:
    x = fields['course'].value
    print(x)
print("</body></html>")
