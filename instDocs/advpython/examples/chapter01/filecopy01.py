#!/usr/bin/env python3
import sys

file_in = open(sys.argv[1], "r")
file_out = open(sys.argv[2], "w")

while True:
    line = file_in.readline()
    if not line: break
    file_out.write(line)
    
file_in.close()
file_out.close()
