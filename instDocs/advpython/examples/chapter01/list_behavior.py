#!/usr/bin/env python3
lis = ["A", "B", "C", "B", "D", "A"]
fmt = "{0:>32s} {1:s}"
print(fmt.format("Original:", lis))
print(fmt.format("Pop Last:", lis.pop()))
print(fmt.format("Pop pos# 2:", lis.pop(2)))
print(fmt.format("Resulting List:", lis))

lis.append(["X", "Y", "Z"])
print(fmt.format("After Append:", lis))
lis.pop()
lis.extend(["X", "Y", "Z"])
print(fmt.format("After Extend:", lis))
