#!/usr/bin/env python3
a = {3, 4, 5}
b = {4, 5, 6, 7}
print("Set a:", a)
print("Set b: ", b)

print("a | b:", a | b )  # union          {3,4,5,6,7}
print("a & b:", a & b )  # intersection         {4,5}
print("a - b:", a - b )  # difference             {3}
print("b - a:", b - a )  # difference           {6,7}
print("a ^ b:", a ^ b )  # symmetric diff     {3,6,7}
