#!/usr/bin/env python3
def copies(data, qty=5, delim=", "):
    for i in range(qty):
        if i == qty - 1:
            delim = ""
        print(str(data), end=delim)
    print()

copies("abc")
copies("xyz", 3)
copies("Hello", 2, "|")
copies([1,2,3], delim=" ~ ")
