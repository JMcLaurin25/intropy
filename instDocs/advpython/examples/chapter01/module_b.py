#!/usr/bin/env python3
def function_three():
    print("In function_three of module:", __name__)

def function_four():
    print("In function_four of module:", __name__)

def common():
    print("In common of module:", __name__)

if __name__ == "__main__":
    function_three()
    function_four()
    common()
