#!/usr/bin/env python3
def total(*numbers):
    print("Debug:", type(numbers))
    sum = 0
    for num in numbers:
        sum += num
    return sum

def sample(val, **items):
    print("Debug:", type(items))
    for key in items:
        items[key] *= val
    return items

print(total(1,2,3))
print()
data = sample(5, key1= 5, key2 = 10, key3 = 20)
print(data)
