#!/usr/bin/env python3
s = "Hello"
print("Looping through a string:", s)
for char in s:
    print(char, end= " ")

data = [102, "some text", {'a':5, 'b':6}]
print("\n\nLooping through a list:", data)
for item in data:
    print(item)

data = tuple(data)
print("\nLooping through a tuple:", data)
for item in data:
    print(item)

r = range(1, 30, 3)
print("\nLooping through a range:", r)
for number in r:
    print(number, end= " ")
print()
