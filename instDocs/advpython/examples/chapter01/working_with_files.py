#!/usr/bin/env python3
import os, sys
filename = sys.argv[0]
current = os.getcwd()
print(current)
os.chdir("..")
print(os.getcwd())
os.chdir(current)
print(os.getcwd())
print("Is . a Directory?", os.path.isdir("."))
print("Is . a File?", os.path.isfile("."))
print("Does", filename, "exist?",
      os.path.exists(filename))
abspath = os.path.abspath(filename)
print(abspath)
print(os.path.split(abspath))
print("Size in bytes", os.path.getsize(abspath))
