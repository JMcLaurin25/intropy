#!/usr/bin/env python3

def sum_of_squares(a,b):
    return a * a + b * b

anonymous = lambda a, b: a * a + b * b

print(type(sum_of_squares))
print(type(anonymous))
print(sum_of_squares(2,3))
print(anonymous(2,3))
