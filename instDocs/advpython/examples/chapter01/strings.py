#!/usr/bin/env python3
import string

print("# of symbols in string:", len(dir(string)))
print(dir(string))
print()
print(string.hexdigits)
print(string.octdigits)
print(string.ascii_lowercase)
print(string.capwords('how are you today',' '))
