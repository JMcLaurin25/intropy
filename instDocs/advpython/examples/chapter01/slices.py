#!/usr/bin/env python3
text = "This is a sample string"
s1 = text[0]               # "T"
s2 = text[5:8]             # "is "
s3 = text[4:]              # " is a sample string"
s4 = text[:4]              # "This"
s5 = text[-1]              # "g"
s6 = text[-3:-1]           # "in"

fmt = "{0}|{1}|{2}|{3}|{4}|{5}"
print(fmt.format(s1, s2, s3, s4, s5, s6))
