#!/usr/bin/env python3
# type bool
a = 5
print(a == 5)
print(a < 5)
x = a == 5
print(type(x))
print(x + x)

# type int
x = 22
y = 7
print(x/y)
print(x // y)
result = divmod(x,y)
print(result)

# type float
x = 1.2
y = 3.4
print(x + y)
print(2/3)
