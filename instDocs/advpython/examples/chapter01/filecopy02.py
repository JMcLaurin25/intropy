#!/usr/bin/env python3
import sys

file_in = open(sys.argv[1], "r")
file_out = open(sys.argv[2], "w")
for line in file_in:
    print(line, end="", file=file_out)

file_in.close()
file_out.close()
