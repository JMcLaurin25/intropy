#!/usr/bin/env python3
x = 12
print(type(x), end=" ")
x = 12.5
print(type(x), end=" ")
x = "34"
print(type(x))

x = "abc"
y = "abc"
z = "a" + "b" + "c"
print(id(x), ":", id(y), ":", id(z))
z = z + "d"
print(id(z))
z[0] = "A"
