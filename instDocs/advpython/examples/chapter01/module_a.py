#!/usr/bin/env python3
def function_one():
    print("In function_one of module:", __name__)

def function_two():
    print("In function_two of module:", __name__)

def common():
    print("In common of module:", __name__)

if __name__ == "__main__":
    function_one()
    function_two()
    common()
