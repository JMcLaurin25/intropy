#!/usr/bin/env python3
value = int(input("Please enter an integer"))
count  = 0
print("Dec:", value, "  Hex:", hex(value))
print("Oct:", oct(value),"  Bin:", bin(value))
while value:
    result = value & 1
    fmt = "{0:08b} & {1:08b} =  {2}"
    txt = fmt.format(value, 1, result)
    print(txt)
    if value & 1:  # bitwise AND
        count += 1
    value = value >> 1 # bit shift right
print("# of set bits:", count)
