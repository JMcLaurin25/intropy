#!/usr/bin/env python3
def modify(a_list, a_function):
    for i, value in enumerate(a_list):
        a_list[i] = a_function(a_list[i])

def other(value, func = (lambda x: x * x)):
    return func(value)

x = [1,2,3]
# pass anonymous function as argument
modify (x, lambda a: a + a)
print(x)

# pass different function as argument
anon = lambda val: bin(val)
modify (x, anon)
print(x)


y= 9
# rely on default function
print(other(y))
# pass optional function as  2nd argument
print(other(y, anon))
