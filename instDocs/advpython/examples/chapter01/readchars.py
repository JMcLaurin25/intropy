#!/usr/bin/env python3
import sys
while True:
    line = sys.stdin.read(1)
    if not line: break
    sys.stdout.write(line)
