#!/usr/bin/env python3
import module_a
from module_b import *

print(__name__)
module_a.function_one()
module_a.function_two()
function_three()
function_four()

import module_b
from module_a import *
common()
module_a.common()
module_b.common()
