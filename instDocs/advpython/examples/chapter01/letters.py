#!/usr/bin/env python3
import sys, string
letters = {}

print("Please enter some text. When done enter:",
      "Ctrl-d in Linux or Ctrl-z in Windows.")
while True:
    line = sys.stdin.readline()
    if not line: break
    for char in line:
        if char in string.ascii_letters:
            currentval = letters.get(char, 0)
            letters[char] = currentval + 1
print()
keys = list(letters.keys())
keys.sort()
for index, key in enumerate(keys):
    print("{0}:{1:02}".format(key,
        letters[key]), end="  ")
    if index % 10 == 9:
        print()
print()
