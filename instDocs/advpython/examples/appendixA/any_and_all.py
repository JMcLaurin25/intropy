#!/usr/bin/env python3

lis = [ {}, [], (), ""]
print("List Contents:", lis)
print("ANY: ", any(lis))
print("ALL: ", all(lis))

lis.append("0")
lis.append([3,4])
print("List Contents:", lis)
print("ANY: ", any(lis))
print("ALL: ", all(lis))
