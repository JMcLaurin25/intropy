#!/usr/bin/env python3
import zlib

f = open("data_compression.py", "rb")
data = f.read()
f.close()
before = len(data)
compressed = zlib.compress(data)
after = len(compressed)
print(before, ":", after, ":",
      (before - after) / before  * 100,
      "% compression")

print("\nOriginal Contents:\n")
print(zlib.decompress(compressed).decode())
