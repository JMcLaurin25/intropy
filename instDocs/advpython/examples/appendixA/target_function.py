#!/usr/bin/env python3
import threading, time
def countdown(count):
    while count > 0:
        print("counting down", count)
        count -= 1
        time.sleep(2)

t1 = threading.Thread(target=countdown, args=(5,))
t1.start()
print("main thread ending.")
