#!/usr/bin/env python3
import threading, time
x = 0
loc = threading.Lock()
def foo():
    global x
    for i in range(1000000):
        loc.acquire()
        x += 1
        loc.release()
def bar():
    global x
    for i in range(1000000):
        loc.acquire()
        x -= 1
        loc.release()

t1 = threading.Thread(target=foo)
t2 = threading.Thread(target=bar)
t1.start()
t2.start()
t1.join()
t2.join()
print(x)
