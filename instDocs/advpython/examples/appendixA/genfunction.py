#!/usr/bin/env python3

def reverse(data):
    for index in range(len(data)-1, -1, -1):
        yield data[index]

#automatic calls to __next__
r = reverse('golf')
for char in r:
    print(char)

# manual calls to __next__
r = reverse('golfcart')
print(next(r), next(r), next(r), next(r))
print(next(r), next(r), next(r), next(r))
