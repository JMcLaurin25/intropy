#!/usr/bin/env python3
import threading, time
class MyThread(threading.Thread):
    def __init__(self, theChar):
        threading.Thread.__init__(self)
        self.x = theChar
    def run(self):
        for ch in range(5):
            print(self.x, "from", \
                  self.getName())
            time.sleep(2)

t1 = MyThread('a')
t1.start()
t2 = MyThread('b')
t2.start()
print("main thread ending!")
