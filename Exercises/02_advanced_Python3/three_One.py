#!/usr/bin/env python3
#import os
from tkinter import *
from tkinter.messagebox import *

def center(mainframe, width, height):
    """ This is used to center the window frame on the screen """
    screen_width = mainframe.winfo_screenwidth()
    screen_height = mainframe.winfo_screenheight()
    #print(screen_width, screen_height)
    centerX = int((screen_width - winX) / 2)
    centerY = int((screen_height - winY) / 2)
    master.geometry(str(winX) + "x" + str(winY) + "+" + str(centerX) + "+" + str(centerY))

def addElem():
    try:
        num1 = int(num1Ent.get())
        num2 = int(num2Ent.get())
    except:
        showinfo("Error", "Invalid Entr(y/ies)")

    ans = num1 + num2
    ans1Ent.delete(0, END)
    ans1Ent.insert(0, ans)

def multElem():
    try:
        num1 = int(num1Ent.get())
        num2 = int(num2Ent.get())
    except:
        showinfo("Error", "Invalid Entr(y/ies)")

    ans = num1 * num2
    ans1Ent.delete(0, END)
    ans1Ent.insert(0, ans)

master = Tk()

winX = 200
winY = 200
center(master, winX, winY)

num1Label = Label(master, text="Enter first number")
num2Label = Label(master, text="Enter second number")
ans1Label = Label(master, text="Answer")

num1Ent = Entry(master, width=20)
num2Ent = Entry(master, width=20)
ans1Ent = Entry(master, width=20)

addBut = Button(master, text="ADD", width=20, command=addElem)
multBut = Button(master, text="MULT", width=20, command=multElem)
exiBut = Button(master, text="exit", width=20, command=master.destroy)

num1Label.pack()
num1Ent.pack()
num2Label.pack()
num2Ent.pack()
ans1Label.pack()
ans1Ent.pack()

addBut.pack()
multBut.pack()
exiBut.pack()

num1Ent.focus_set()

master.mainloop()
