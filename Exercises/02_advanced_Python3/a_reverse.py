#!/usr/bin/env python3
class Reverse:
    """Iterator: loops backwards over seq """
    def __init__(self, data):
        self.data = data
        self.index = len(data)

    def __iter__(self):
        return self

    def __next__(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]

#Automatic call to __next__
r = Reverse('spam')
for char in r:
    print(char)
#Manual calls to __next__
r = Reverse('spamalot')
print(next(r), next(r), next(r), next(r))
print(next(r), next(r), next(r), next(r))
