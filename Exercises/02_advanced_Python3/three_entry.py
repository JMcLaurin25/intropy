#!/usr/bin/env python3
import os
from tkinter import *

def center(mainframe, width, height):
    """ This is used to center the window frame on the screen """
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    #print(screen_width, screen_height)
    centerX = int((screen_width - winX) / 2)
    centerY = int((screen_height - winY) / 2)
    root.geometry(str(winX) + "x" + str(winY) + "-" + str(centerX) + "-" + str(centerY))

def listfiles():
    print("Listing Files from " + fileEntry.get())
    files = os.listdir(fileEntry.get())
    for file in files:
        print(file)
    print("+" * 20)
def dele():
    fileEntry.delete(0, 'end')
def stop():
    root.destroy()

root = Tk()

winX = 300
winY = 125
center(root, winX, winY)

root.title("Directories")
dirTitle = Label(root, text="Directory Name:")
fileEntry = Entry(root, width=30)
fileBut = Button(root, text="List Files", width=10, command=listfiles)
quitBut = Button(root, text="Quit", width=10, command=stop)
deleBut = Button(root, text="Delete Text", width=10, command=dele)

dirTitle.pack(side=TOP)
fileEntry.pack()
fileBut.pack()
quitBut.pack()
deleBut.pack()

fileEntry.focus_set()

root.mainloop()
