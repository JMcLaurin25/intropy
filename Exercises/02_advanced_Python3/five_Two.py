"""
Repeat(1)
	- Print the files in order of the modification date.
"""
#!/usr/bin/env python3
import os, sys, time
fileDict = {}
dirFiles = os.popen("ls")
for doc, value in enumerate(dirFiles):
	file = value[:-1]
	info = os.stat(file)
	modDate = time.ctime(info.st_mtime)
	#print(modDate, type(modDate))
	fileDict[file] = modDate
keys = list(fileDict.keys())
keys.sort(key= lambda x: fileDict[x])
for file in keys:
	fmt = ("%30s: %s")
	print(fmt % (file, fileDict[file]))
