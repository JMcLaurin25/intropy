#!/usr/bin/env python3
from tkinter import *
from tkinter.messagebox import *

def center(mainframe, winX, winY):
    """ This is used to center the window frame on the screen """
    screen_width = mainframe.winfo_screenwidth()
    screen_height = mainframe.winfo_screenheight()
    centerX = int((screen_width - winX) / 2)
    centerY = int((screen_height - winY) / 2)
    mainframe.geometry(str(winX) + "x" + str(winY) + "+" + str(centerX) + "+" + str(centerY))

def addElems():
    try:
        num1 = int(num1Ent.get())
        num2 = int(num2Ent.get())
    except:
        showinfo("Error", "Invalid Entr(y/ies)")

    ans = num1 + num2
    ans1Ent.delete(0, END)
    ans1Ent.insert(0, ans)
    calcHist.append("\n" + str(num1) + " + " + str(num2) + " = " + str(ans))

def multElems():
    try:
        num1 = int(num1Ent.get())
        num2 = int(num2Ent.get())
    except:
        showinfo("Error", "Invalid Entr(y/ies)")

    ans = num1 * num2
    ans1Ent.delete(0, END)
    ans1Ent.insert(0, ans)
    calcHist.append("\n" + str(num1) + " x " + str(num2) + " = " + str(ans))

def raiseElems():
    try:
        num1 = int(num1Ent.get())
        num2 = int(num2Ent.get())
    except:
        showinfo("Error", "Invalid Entr(y/ies)")

    ans = pow(num1, num2)
    ans1Ent.delete(0, END)
    ans1Ent.insert(0, ans)
    calcHist.append("\n" + str(num1) + " ^ " + str(num2) + " = " + str(ans))

def clearElems():
    num1Ent.delete(0, END)
    num2Ent.delete(0, END)
    ans1Ent.delete(0, END)

def dispHist():
    dispPop = Toplevel(root)
    dispPop.title("History")
    center(dispPop, 300, 250)
    dispTxt = Text(dispPop)
    dispTxt.pack()
    for line in calcHist:
        dispTxt.insert(END, line)

root = Tk()
root.title("Calculations")
calcHist = []

winX = 275
winY = 190
center(root, winX, winY)

#Menu below---------------------------------------------
menubar = Menu(root)
root.config(menu=menubar)

functionmenu = Menu(menubar)
functionmenu.add_command(label="Clear", command=clearElems)
functionmenu.add_separator()
functionmenu.add_command(label="Add", command=addElems)
functionmenu.add_command(label="Mult", command=multElems)
functionmenu.add_command(label="Raise", command=raiseElems)

menubar.add_cascade(label="Functions", menu=functionmenu)

exitmenu = Menu(menubar)
exitmenu.add_command(label="Close", command=root.destroy)

menubar.add_cascade(label="Exit", menu=exitmenu)

#Button frame below-------------------------------------
buttFrame = Frame(root)

headLabel = Label(buttFrame, text="Select from the following choices").pack(side=TOP)
addBut = Button(buttFrame, text="add", command=addElems).pack(side=LEFT)
multBut = Button(buttFrame, text="mult", command=multElems).pack(side=LEFT)
raiseBut = Button(buttFrame, text="raise", command=raiseElems).pack(side=LEFT)
exitBut = Button(buttFrame, text="exit", command=root.destroy).pack(side=LEFT)
clearBut = Button(buttFrame, text="clear", command=clearElems).pack(side=LEFT)

#Entry frame below--------------------------------------
entryFrame = Frame(root)

Label(entryFrame, text="First Value Here").pack()
num1Ent = Entry(entryFrame, width=20)
num1Ent.pack()
Label(entryFrame, text="Second Value Here").pack()
num2Ent = Entry(entryFrame, width=20)
num2Ent.pack()
Label(entryFrame, text="Answer Here").pack()
ans1Ent = Entry(entryFrame, width=20)
ans1Ent.pack()

#Display frame below------------------------------------
dispFrame = Frame(root)

dispBut = Button(dispFrame, text="Display History", command=dispHist).pack(fill=BOTH)

num1Ent.focus_set()

buttFrame.pack()
entryFrame.pack()
dispFrame.pack(fill=BOTH)
root.mainloop()
