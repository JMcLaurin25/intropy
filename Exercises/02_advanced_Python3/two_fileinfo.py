#!/usr/bin/env python3
import os
files = os.listdir(".")
fileinfo = {f : os.path.getsize(f) for f in files}
#print(fileinfo)

for name, size in fileinfo.items():
	print("{0:30} : {1:>6} bytes".format(name,size))
