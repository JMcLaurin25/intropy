#!/usr/bin/env python3
from one_Two import SortedArray

while True:
	userIn = input("Enter number, (q) to quit:")
	if userIn.lower() == "q":
		break
	if userIn.isdigit():
		num = int(userIn)
		randList = [4,2,5,7,9,1,8]
		newArray = SortedArray(randList)

		print("Add", num, "to array", newArray.addto(num))
		print("Remove first element: ", newArray.shiftto())
		print("Remove last element: ", newArray.popoff())
		print("Length of array:", len(newArray))
		print("Print out of array:", newArray)
