#!/usr/bin/env python3
alist = [2,4,6]
result = [3 * x for x in alist]
print(result)
result = [3 * x for x in [1,2,3,4,5]]
print(result)
result = [3 * x for x in range(1,5)]
print(result)
result = [ [x, 3 * x] for x in range(1,5)]
print(result)

