#!/usr/bin/env python3
from tkinter import *
from tkinter.messagebox import *

def center(mainframe, width, height):
    """ This is used to center the window frame on the screen """
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    #print(screen_width, screen_height)
    centerX = int((screen_width - winX) / 2)
    centerY = int((screen_height - winY) / 2)
    root.geometry(str(winX) + "x" + str(winY) + "-" + str(centerX) + "-" + str(centerY))

def notyet():
    print("This event handler does nothing yet")

def go():
    print("Go")

def callback():
    if askyesno('Verify', 'Really Quit?'):
        exit(0)

root = Tk()

winX = 300
winY = 50
center(root, winX, winY)

menubar = Menu(root)
root.config(menu=menubar)

filemenu = Menu(menubar)
filemenu.add_command(label="New", command=notyet)
filemenu.add_separator()
filemenu.add_command(label="Save", command=notyet)
filemenu.add_command(label="SaveAs", command=notyet)

menubar.add_cascade(label="File", menu=filemenu)

editmenu = Menu(menubar)
editmenu.add_command(label="Cut", command=notyet)
editmenu.add_command(label="Copy", command=notyet)
editmenu.add_command(label="Paste", command=notyet)

menubar.add_cascade(label="Edit", menu=editmenu)

inside = Frame(root)
quitBut = Button(inside, text="Quit", width=10, command=callback)
contBut = Button(inside, text="Keep Going", width=10, command=go)

inside.pack()
quitBut.grid(column=0, row = 0, sticky=E)
contBut.grid(column=1, row = 0, sticky=E)


root.mainloop()
