#!/usr/bin/env python3
import os, sys

while True:
	fileIn = input("Enter filename, (q) to quit: ")
	if fileIn.lower() == 'q':
		break
	
	fmt = ("%20s: %s")
	if os.path.isfile(fileIn):
		print("Is a file")
		filename = fileIn
		filestats = os.stat(fileIn)
		filesize = filestats.st_size

		fopened = open(filename, 'r')
		lines = fopened.readlines()
		lineLen = len(lines)
		wordNum = 0
		for line in lines:
			words = line.split()
			wordNum += len(words)

		print(fmt % ("Filename", filename))
		print(fmt % ("Filesize (bytes)", filesize))
		print(fmt % ("Line number", lineLen))
		print(fmt % ("Word number", wordNum))
