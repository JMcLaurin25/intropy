#!/usr/bin/env python3
import os
from tkinter import *

def center(mainframe, width, height):
    """ This is used to center the window frame on the screen """
    screen_width = master.winfo_screenwidth()
    screen_height = master.winfo_screenheight()
    #print(screen_width, screen_height)
    centerX = int((screen_width - winX) / 2)
    centerY = int((screen_height - winY) / 2)
    master.geometry(str(winX) + "x" + str(winY) + "-" + str(centerX) + "-" + str(centerY))

def getselected():
    item = listbox.curselection()
    li = list(item)
    if not li:
        pritn("No Selection.")
        return
    for i in li:
        print(listbox.get(i))

master = Tk()

winX = 200 
winY = 215

center(master, winX, winY)

headerLabel = Label(master, text="Selection from List")
listbox = Listbox(master, selectmode=MULTIPLE)
selBut = Button(master, text="Get Selection", command=getselected)

for item in os.listdir("."):
    listbox.insert(END, item)

headerLabel.pack()
listbox.pack()
selBut.pack()

master.mainloop()
