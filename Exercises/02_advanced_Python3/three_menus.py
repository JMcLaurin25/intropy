#!/usr/bin/env python3
from tkinter import *

def center(mainframe, width, height):
    """ This is used to center the window frame on the screen """
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    #print(screen_width, screen_height)
    centerX = int((screen_width - winX) / 2)
    centerY = int((screen_height - winY) / 2)
    root.geometry(str(winX) + "x" + str(winY) + "-" + str(centerX) + "-" + str(centerY))

def notyet():
    print("This event handler does nothing yet")

root = Tk()

winX = 800
winY = 400
center(root, winX, winY)

menubar = Menu(root)
root.config(menu=menubar)

filemenu = Menu(menubar)
filemenu.add_command(label="New", command=notyet)
filemenu.add_separator()
filemenu.add_command(label="Save", command=notyet)
filemenu.add_command(label="SaveAs", command=notyet)

menubar.add_cascade(label="File", menu=filemenu)

editmenu = Menu(menubar)
editmenu.add_command(label="Cut", command=notyet)
editmenu.add_command(label="Copy", command=notyet)
editmenu.add_command(label="Paste", command=notyet)

menubar.add_cascade(label="Edit", menu=editmenu)




root.mainloop()
