#!/usr/bin/env python3
def oddints(upper):
	print("inside of oddints()")
	for i in range(1, upper, 2):
		print("Before yield")
		yield i
		print("After yield")
gen = oddints(5)
print(type(gen))
print(next(gen))
print("Between next() calls")
print(next(gen))
