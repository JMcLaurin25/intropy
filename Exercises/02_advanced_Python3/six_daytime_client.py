#!/usr/bin/env python
import sys, socket

host = 'localhost'
port = 3009
buffersize = 1024
if len(sys.argv) == 3:
    host = sys.argv[1]
    port = int(sys.argv[2])

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host,port))
data = s.recv(buffersize)

print(type(data), data)
print('Received: SUCKA!', data.decode())
s.close()
