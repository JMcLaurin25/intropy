#!/usr/bin/env python3
from socket import *

host = '0.0.0.0'
port = 5556

s = socket(AF_INET, SOCK_STREAM)

s.bind(host, port)
s.listen(5)

while True:
    try:
        print("Waiting for connection...")
        client, client_addr = s.accept()
        print(client + " connected.")
    except KeyboardInterrupt:
        break
    except Exception as err:
        print(err)

print("Server shutting down")
s.shutdown(SHUT_RDWR)
s.close()
