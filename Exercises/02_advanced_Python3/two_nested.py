#!/usr/bin/env python3
words = ["hello", "racecar", "eye", "bike", "stats", "civic"]
pairs = [[word.upper(), len(word)] for word in words if word[::-1] == word]

print(pairs)
