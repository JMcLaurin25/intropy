#!/usr/bin/env python3
from tkinter import *
from tkinter.colorchooser import askcolor
from tkinter.filedialog import askopenfilename

def center(mainframe, width, height):
    """ This is used to center the window frame on the screen """
    screen_width = master.winfo_screenwidth()
    screen_height = master.winfo_screenheight()
    #print(screen_width, screen_height)
    centerX = int((screen_width - winX) / 2)
    centerY = int((screen_height - winY) / 2)
    master.geometry(str(winX) + "x" + str(winY) + "-" + str(centerX) + "-" + str(centerY))

def setBgColor():
    (triple, hexstr) = askcolor()
    if ( hexstr ):
        print(hexstr, triple)
        btn1.config(bg=hexstr)

def find():
    ans = askopenfilename()
    f = open(ans, "r")
    lines = f.readlines()
    for line in lines:
        t.insert(END, line)

def cls():
    t.delete(1.0, END)

master = Tk()

winX = 400
winY = 200
center(master, winX, winY)

btn1 = Button(master, text="Choose a color", command=setBgColor)
btn1.config(height=2, font=("Courier", 20, 'bold'))

openBut = Button(master, text="Open File", width=20, command=find)
clsBut = Button(master, text="Clear", width=20, command=cls)

t = Text(master, height=10, width=70)

btn1.pack(expand=YES, fill=BOTH)
openBut.pack()
clsBut.pack()
t.pack()

master.mainloop()
