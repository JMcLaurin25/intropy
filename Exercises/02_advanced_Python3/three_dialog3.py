#!/usr/bin/env python3
from tkinter import *
from tkinter.messagebox import *

def center(mainframe, width, height):
    """ This is used to center the window frame on the screen """
    screen_width = master.winfo_screenwidth()
    screen_height = master.winfo_screenheight()
    #print(screen_width, screen_height)
    centerX = int((screen_width - winX) / 2)
    centerY = int((screen_height - winY) / 2)
    master.geometry(str(winX) + "x" + str(winY) + "-" + str(centerX) + "-" + str(centerY))

def cls():
    t.delete('1.0', 'end')
def show():
    filename = e.get()
    try:
        f = open(filename, "r")
        lines = f.readlines()
        for line in lines:
            t.insert('end', line)
    except OSError:
        showerror('ERROR', 'No file ' + filename)

master = Tk()

winX = 800
winY = 175
center(master, winX, winY)

window = Frame(master)
inner = Frame(window)

headLabel = Label(inner, text="Enter File Name")
e = Entry(inner, width=20)
showBut = Button(inner, text="Show File", width=25, command=show)
cleaBut = Button(inner, text="Clear Text", width=25, command=cls)
t = Text(window, height=10,width=70)

headLabel.pack()
e.pack()
showBut.pack()
cleaBut.pack()

t.grid(column=1, row=0)

inner.grid(column=0, row=0)
window.pack()
e.focus_set()

master.mainloop()
