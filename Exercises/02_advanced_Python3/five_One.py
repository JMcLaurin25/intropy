"""
Use 'popen' and the 'ls' command to retrieve file names in the current directory.
	- Print the files in order of the length of their names.
"""
#!/usr/bin/env python3
import os
fileDict = {}
dirFiles = os.popen("ls")
for doc, value in enumerate(dirFiles):
	file = value[:-1]
	filesize = os.path.getsize(file)
	fileDict[file] = filesize
keys = list(fileDict.keys())
keys.sort(key= lambda x: fileDict[x])
for file in keys:
	fmt = ("%30s: %d")
	print(fmt % (file, fileDict[file]))
