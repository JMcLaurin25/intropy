#!/usr/bin/env python3
import os, sys
usrArgs = sys.argv
usrCmds = [cmd for cmd in usrArgs if usrArgs.index(cmd) != 0]

#print(usrCmds)

exist_pid = os.fork()
#print(exist_pid)

if exist_pid > 0:
	os.wait()
elif exist_pid == 0:
	for cmd in usrCmds:
		dataStream = os.popen(cmd)
		for data in dataStream:
			print(data, end="")
else:
	print("Forking Error. Fork Yo Momma!")
