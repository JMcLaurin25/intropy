"""
Create a dictionary of dictionaries from 'dataset'
"""
#!/usr/bin/env python3
import sys

def fileIn(fileName):
	file = open(fileName, "r")
	info = file.readlines()
	file.close()
	for line in info:
		cleanLine = line[:-1]
		indx = info.index(line)
		info[indx] = cleanLine
	return info

def splitLine(line):
	return line.rstrip().split()

info = fileIn("dataset")

userDict = {}
for i in info:
	line = (splitLine(i))
	userDict.update({line[0]:{}})

for data in info:
	line = (splitLine(data))
	user = line[0]
	subDict = userDict.get(user)
	newvalue = int(subDict.get(line[1], 0)) + int(line[2])
	subDict.update({ line[1]:newvalue})
	userDict.update({ user:subDict })

for key in userDict.keys():
	print(key, userDict[key])
