#!/usr/bin/env python3
import os
files_dict = {}
files = os.listdir(".")
for index, value in enumerate(files):
	files_dict[value] = os.path.getsize(value)
#print(files_dict)
keys = list(files_dict.keys())
keys.sort(key=lambda x : files_dict[x])

for file in keys:
	print("%-25s %5d" % (file, files_dict[file]))
