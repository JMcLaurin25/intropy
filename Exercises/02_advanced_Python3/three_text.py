#!/usr/bin/env python3
#import os
from tkinter import *

def center(mainframe, width, height):
    """ This is used to center the window frame on the screen """
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    #print(screen_width, screen_height)
    centerX = int((screen_width - winX) / 2)
    centerY = int((screen_height - winY) / 2)
    root.geometry(str(winX) + "x" + str(winY) + "-" + str(centerX) + "-" + str(centerY))

def grabtxt():
    line = ent01.get()
    pos = ent02.get()
    data = txt.get(line + "." + pos, END)
    print(data)

def deltxt():
    line = ent01.get()
    pos = ent02.get()
    txt.delete(line + "." + pos, 'end')

root = Tk()

winX = 420
winY = 300

center(root, winX, winY)

startLabel = Label(root, text="Starting Line Number:")
ent01 = Entry(root)
startPos = Label(root, text="Starting pos within line")
ent02 = Entry(root)
b1 = Button(root, text="Grab Text", width=10, command="grabtxt")
b2 = Button(root, text="Delete Text", width=10, command="deltxt")
b3 = Button(root, text="Quit", width=10, command=root.destroy)
txt = Text(root, height=8, width=50)
txt.insert("end", "This is line 1\nThis is line 2")

startLabel.pack()
ent01.pack()
startPos.pack()
ent02.pack()
b1.pack()
b2.pack()
b3.pack()
txt.pack()

root.mainloop()
