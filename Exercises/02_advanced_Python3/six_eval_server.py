#!/usr/bin/env python3
from socket import *
from six_handler_function import handle_client

host = '0.0.0.0'
port = 2345

s = socket(AF_INET, SOCK_STREAM)
s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
s.bind((host,port))
s.listen(5) # max num of queued connections

while True:
    try:
        print("Waiting for connection...")
        client, client_addr = s.accept()
        print("Connection from...", client_addr)
        handle_client(client)
    except KeyboardInterrupt:
        break
    except Exception as err:
        print(err)

print("\nShutting down the server")
s.shutdown(SHUT_RDWR)
