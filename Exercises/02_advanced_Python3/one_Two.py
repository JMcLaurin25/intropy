#!/usr/bin/env python3

class SortedArray:
	def __init__(self, list):
		self.list = list

	def addto(self, newNum):
		#print(self.list)
		self.list.append(newNum)
		self.list.sort()
		#print(self.list)
		return self.list

	def shiftto(self):
		popped = self.list.pop(0)
		#print(self.list)
		return popped

	def popoff(self):
		popped = self.list.pop(len(self.list)-1)
		#print(self.list)
		return popped

	def __len__(self):
		return len(self.list)

	def __str__(self):
		strOut = ""
		for i in self.list:
			strOut += str(i)
		return strOut
