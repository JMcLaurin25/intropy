"""
Create a list of four elements.
	- The elements should be an int, a string, a list, and a dictionary.
	- Then, write a subroutine that loops through the list and prints each element.
	- The functions should also determine the elements type and print it.
		* The built-in 'isinstance' function may come in handy here.
"""
#!/usr/bin/env python3

data = [7, "Batman", ["Milk", "Cookies"], {"First Name": "Billie", "Last Name": "Jean"}]

data_items = [type(x) for x in data]
subList_items = [x for x in data if isinstance(x, list)]

def runthrough(list):
	"""yield [str(x) for x in data if isinstance(x, int)]
	yield [x for x in data if isinstance(x, str)]
	for x in data:
		if x is list:
			for y in x:
				yield (y)
		elif isinstance(x, dict):
			tempList = []
			for y in x:
				tempList.append(y)
				tempList.append(x[y])
			for val in tempList:
				yield val"""
	for x in data:
		print(type(x))
		if isinstance(x, int):
			yield (str(x))
		elif isinstance(x, str):
			yield (x)
		elif x is list:
			for y in x:
				yield (y)
		elif isinstance(x, dict):
			tempList = []
			for y in x:
				tempList.append(y)
				tempList.append(x[y])
			for val in tempList:
				yield val


for item in runthrough(data):
	print("{0:15s}: {1}".format(item, type(item)))
