#!/usr/bin/env python3

def get_customers():
	"""Gets the customer data from file."""
	custFile = open("customers.txt", "r")
	custList = custFile.readlines()
	custFile.close()
	# The following line creates a nestedlist of the customers data, removes the end whitespace, then splits the elements of the nestedlist by ','.
	return [customer.rstrip().split(",") for customer in custList]

def get_info(nestedList):
	info = ""
	for i in range(len(nestedList)):
		info += str(type(nestedList[i])) + "\n"
		#print(nestedList[i])
	return info[:-1]

def get_dictionary(nestedList):
	return {cust[0] + " " + cust[1]: cust for cust in nestedList}
	#for cust in nestedList:
	#	print(cust[0] + " " + cust[1])

def user_interaction(customers):
	tags = ["FirstName", "LastName", "Street", "City", "State", "Zipcode"]
	while True:
		name = input("Enter a customer's name:")
		if name.lower() == "quit" or name.lower() == "q": break
		try:
			names = name.lower().split()
			fullName = names[0] + " " + names[1]
			cleanedName = fullName.title()
			data = customers[cleanedName]	
			for i, value in enumerate(data):
				fmt = ("%10s: %s")
				print(fmt % (tags[i], value))
		except KeyError:
			print("Customer name not found.")
		except IndexError:
			print("Provide First and Last names.")
			if len(name) == 0:
				print("No entry submitted.")


def print_customers(customer_map):
	print("Here are the customers names:")
	for i, customer in enumerate(customer_map):
		print("{0:20}".format(customer), end = "")
		if i % 4 == 3:
			print()
	print("\n")
