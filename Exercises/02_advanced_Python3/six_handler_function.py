#!/usr/bin/env python3
from socket import *
def handle_client(client):
    while True:
        data = client.recv(1024)
        if not data:
            break
        try:
            result = str(eval(data.decode()))
            client.send(result.encode())
        except:
            client.send(b'Invalid Expression')
    client.shutdown(SHUT_RDWR)
    client.close()
