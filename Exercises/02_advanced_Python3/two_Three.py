"""
Write list comprehensions for the following.
	- A list of elements 0,1,2,3,4.....99
	- A list from the above comprehension of those values that are evenly divisible by 5

	* Should use a nested comprehension.
"""
#!/usr/bin/env python3

print("Full list:\n", [x for x in range(100)])
print("List, divisible by 5:\n",[y for y in ([x for x in range(100)]) if y % 5 == 0])
