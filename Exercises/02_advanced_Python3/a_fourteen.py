#!/usr/bin/env python3
import signal

def handler(signal, other):
    print("\nReceived Signal: ")
    exit(1)

while True:
    signal.alarm(2)
    signal.signal(signal.SIGALRM, handler)
    x = input("input a number: ")
    signal.alarm(0)
    print("You input", x)
