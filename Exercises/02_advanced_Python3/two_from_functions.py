#!/usr/bin/env python3
names = ["Ashley","Emma","Jayden","Ethan"]
lengths = [len(name) for name in names]
print(lengths)

pairs = [[name, len(name)] for name in names]
print(pairs)

grades = [[88,77,99], [95,98,97], [70,100,95]]
highest = [max(grade) for grade in grades]
print(highest)
