#!/usr/bin/env python3
a = [1,2,3]
b = list(a) # Deep copy
print(id(a))
print(id(b))

a.append(10)
print(a)
print(b)
