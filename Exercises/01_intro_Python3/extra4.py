#!/usr/bin/env python3
"""
Input a string and determine whether it is spelled the same forward and backward. In other words, is it a palindrome?
"""
while True:
	entry = input("Enter word:")
	reverse = entry[::-1]
	fmt = ("%15s %s") 
	print(fmt % ("Word:", entry))
	print(fmt % ("Reverse:", reverse))
	print()

	if reverse == entry:
		print(entry, "IS a palindrome.")
	else:
		print(entry, "is NOT a palindrome.\n")
	if entry.lower() == "exit":
		break
