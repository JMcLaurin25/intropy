#!/usr/bin/env python3

while True:
	num1 = int(input("Enter first number:"))
	num2 = int(input("Enter second number:"))

	if num1 == num2:
		print("Numbers are equal.")
	else:
		print("Numbers are not equal.")
