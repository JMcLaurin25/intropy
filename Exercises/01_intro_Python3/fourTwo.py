#!/usr/bin/env python3

masterSet = set()
while True:
	data = input("Enter a line (q to quit) :")
	if data == "q":
		break

	#process lines here
	linewords = data.split()
	for word in linewords:
		masterSet.add(word)
	

setList = list(masterSet)
setList.sort()
print(setList)
uniqueWds = len(setList)
print("There were", uniqueWds, "unique words entered")
