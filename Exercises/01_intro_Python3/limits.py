#!/usr/bin/env python3

lowerLim = int(input("Enter lower limit:"))
upperLim = int(input("Enter upper limit:"))
step = int(input("Enter step value:"))

for i in range(lowerLim, upperLim, step):
	print(i)
