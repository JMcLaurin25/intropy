"""
Write a program that uses a loop to prompt the user and get an integer value.
	The program should print the sum of all the integers entered.
	- If the user enters a blank line or any other line that cannot be converted to an integer, the program should handle this 'ValueError'.
	- If the user uses Ctrl-C to terminate the program, it should be trapped with a 'KeyboardInterrupt', and a suitable message should be printed.
	- When the user enters the end of file character (Ctrl-D on Linux or Ctrl-Z on Windows), the program should trap this with the 'EOFError' and break out of the loop and print the sum of all the integers.
"""
#!/usr/bin/env python3
total = 0
while True:
	
	try:
		userIn = input("Enter an integer, (q) to quit: ")
		if userIn.lower() == 'q' or userIn.lower() == 'quit':
			break
		if userIn == " " or userIn == "":
			raise ValueError("Blank Entry")		
		userInt = int(userIn)
		total += userInt
	except ValueError as err:
		print(err)
	except KeyboardInterrupt:
		print("Enter (q) to quit.")
	except EOFError: # Ctrl-D
		break

print("The sum of all integers: ", total)
