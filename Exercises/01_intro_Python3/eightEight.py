"""
Revise your solution to the previous exercise so that if you specify the '-t' option on the command line (before the list of files), your program also prints total number of lines, words, and characters in all the files.
"""
#!/usr/bin/env python3
import sys

def printCts(counts):
	fmt = ("%25s: %s")
	print(fmt %("Number of Lines", counts[0]))
	print(fmt %("Number of words", counts[1]))
	print(fmt %("Number of characters", counts[2]))
	print()

def printTotal(values):
	print("-" * 30)
	print("TOTAL:")
	printCts((values[0],values[1],values[2]))

def processArgs(args):
	finalTotal = False
	totalLns = 0
	totalWds = 0
	totalChrs = 0
	print()
	for arg in range(len(args)- 1):
		if args[arg+1] == "-t":
			finalTotal = True
		else:
			lineCt = len(countLines(args[arg+1]))
			wordCt = len(countWords(args[arg+1]))
			charCt = countChars(args[arg+1])
			counts = (lineCt, wordCt, charCt)
			print("File:", args[arg+1])
			printCts(counts)

			totalLns += lineCt
			totalWds += wordCt
			totalChrs += charCt
	if finalTotal == True:
		printTotal((totalLns, totalWds, totalChrs))

def countLines(file):
	data = open(file, 'r')
	lines = data.readlines()
	data.close()
	for item in lines:
		indx = lines.index(item)
		item = item[:-1]
		lines[indx] = item
	return lines

def countWords(file):
	wordList = []
	lines = countLines(file)
	for line in lines:
		words = line.split(" ")
		for word in words:
			wordList.append(word)
	return wordList

def countChars(file):
	charCt = 0
	words = countWords(file)
	for word in words:
		#print(charCt, len(word))
		charCt += len(word)
	return charCt

def main():
	arguments = (sys.argv)
	processArgs(arguments)


if __name__=="__main__":
	main()
