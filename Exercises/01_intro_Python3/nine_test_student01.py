#!/usr/bin/env python3
from nine_student01 import Student

s1 = Student("Elizabeth", "Electrical Engineering")

print(s1.name())
print(s1.major())
s1.setName("Beth")
s1.setMajor("Computer Science")
print(s1.name())
print(s1.major())
