"""
Create a class called 'Family'
	The 'Family' should be composed of two 'Person' objects representing the parents and a 'list' of 'Person' objects representing the children.
	- Therefore, the '__init__' method should take two required parameters, followed by a variable number of arguments.
	- The first two 'Person' objects will be the parents, and any remaining objects will be children.
	- Use the following to test your classes.
		mom = Person("Mommy", 45, "F")
		dad = Person("Daddie", 45, "M")
		kid1 = Person("Johnie", 2, "M")
		kid2 = Person("Janie", 3, "F")
		myFamily = Family(mom, dad, kid1, kid2)
		kid3 = Person("Paulie", 1, "M")
		myFamily.add(kid3)
		print(myFamily)
"""
#!/urs/bin/env python3

class Family():
	""" Family class """
	def __init__(self, mom, dad, *children):
		self.thismom = mom
		self.thisdad = dad
		self.thesechildren = []
		for child in children:
			self.thesechildren.append(child)

	#--Getters
	def getmom(self):
		return self.thismom
	def getdad(self):
		return self.thisdad
	def getchildren(self):
		return self.thesechildren
	#--Setters
	def setmom(self, newmom):
		self.thismom = newmom
	def setdad(self, newdad):
		self.thisdad = newdad
	def setchildren(self, newchildren):
		self.thesechildren = newchildren

	def add(self, newchild):
		self.thesechildren.append(newchild)

	def __str__(self):
		output = ""
		output+=self.thismom.name() + "\n"
		output+=self.thisdad.name() + "\n"
		for child in self.thesechildren:
			output+=child.name() + "\n"
		return output
	

