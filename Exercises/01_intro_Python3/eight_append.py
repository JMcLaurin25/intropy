#!/usr/bin/env python3
f = open("eight_test.txt", 'a')
while True:
	userIn = input(">> (q) to quit: ")
	if userIn.lower() == 'q' or userIn.lower() == 'quit':
		break
	f.write(userIn + "\n")

f.close()
