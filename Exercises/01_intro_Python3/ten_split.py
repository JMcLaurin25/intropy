#!/usr/bin/en python3

import re
while True:
	line = input("Enter a string: ")
	if line.lower() == 'q':
		break
	total = 0
	x = re.split('\D+', line)

	if x:
		print("Split into a list of length", len(x))
		for num in x:
			if len(num) != 0:
				total += int(num)
		print(total)

