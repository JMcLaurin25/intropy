#!/usr/bin/env python3
"""
Ask the user for three values:
a) The number of rows
b) The number of columns
c) A string
Then print the string as a value in that many rows and columns.
"""

value1 = int(input("Enter number of rows:"))
value2 = int(input("Enter number of columns:"))
value3 = input("Enter a word:")

for i in range(value2):
	print((value3 + " ") * value1)
