"""
Create a list in your program that has 10 numbers.
	Then, in a loop, ask the user for a number.
	- Use this number as an index into your list and print the value located at that index.
	- End the program when the user enters "end".
	- Handle the case of an illegal number.
	- Handle the case of an illegal subscript.
"""
#!/usr/bin/env python3
while True:
	numList = [32,25,83,24,67,36,97,18,90,10]
	userIn = input("Enter a number, (end) to quit:")
	if userIn.lower() == "end":
		break
	try:
		userIn = int(userIn)
		print("The number at", userIn, "is: ", numList[userIn])
	except ValueError:
		print("Value Error: non numeric data")
	except IndexError:
		print("Index Error: number larger than list max index")
