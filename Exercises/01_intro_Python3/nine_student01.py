#!/usr/bin/env python3
class Student:
	"""This is the Student Class file"""
	def __init__(self, name, major):
		self.thename = name
		self.themajor = major

	def name(self):	
		return self.thename
	def major(self):
		return self.themajor

	def setName(self, newname):
		self.thename = newname
	def setMajor(self, newmajor):
		self.themajor = newmajor
