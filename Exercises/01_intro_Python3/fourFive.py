#!/usr/bin/env python3

masterSet = set()
masterList = list()
map1 = {}

def usethis(akey):
	return map1[akey]

while True:
	data = input("Enter a line (q to quit) :")
	if data == "q":
		break

	linewords = data.split()
	for word in linewords:
		masterSet.add(word)
		masterList.append(word)	

for word in masterSet:
	numVal = masterList.count(word)
	#print(numVal, word)
	map1[word] = numVal

keyList = list(map1.keys())
keyList.sort(key=usethis)
for count in keyList:
	fmt = ("%10s : %2d")
	print(fmt % (count, map1[count]))

#print(map1)
