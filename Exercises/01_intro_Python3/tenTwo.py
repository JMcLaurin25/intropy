"""
Write a program that reads a line at a time and determines whether the input consists solely of an integer number that is positive or negative.
	Specify whether it is positive or negative.
"""
#!/usr/bin/en python3

import re
while True:
	line = input("Enter a number: ")
	if line.lower() == 'q':
		break
	if line.isdigit():
		fltNum = float(line)
		line = str(fltNum)
	x = re.match('^(\-?[0-9]+.[0-9]+)$', line)
	if x:
		if float(x.group(0)) > 0.0:
			print("Match is positive", line)
		elif float(x.group(0)) < 0.0:
			print("Match is negative", line)
	else:
		print("Invalid entry >>", line)


