"""
Create a few functions and place them in a file.
	Now, write a Python program that imports the module.
	- Use the functions from the module in this program.
"""
#!/usr/bin/env python3

def forwardOut(var):
	return (var)

def reverseOut(var):
	return (var[::-1])

def multOut(var, count):
	sentence = var * int(count)
	return sentence

if __name__=="__main__":
	print("Modules top-level. Function test")
	print(forwardOut("alphabet"))
	print(reverseOut("alphabet"))
	print(multOut("alphabet", 4))
