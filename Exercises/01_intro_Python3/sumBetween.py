#!/usr/bin/env python3

def main():

	num1 = int(input("Enter first number:"))
	num2 = int(input("Enter second number:"))
	sum = 0

	for i in range(num1, num2+1):
		sum += i
	print("The sum of integers between",num1,"and",num2,"is:",sum)

if __name__ == "__main__":
	main()
