#!/usr/bin/env python3
import os

files = os.listdir(".")
ct = 0 # ct : count of the lines

for file in files:
	f = open(file, "r")
	lines = f.readlines()
	ct += len(lines)
	f.close() # Closes files

print("Number of lines: ", ct)
