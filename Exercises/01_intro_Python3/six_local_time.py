"""
print the separate parts of time struct
"""

#!/usr/bin/env python3
import time
date = time.ctime()
print("Date is: ", date)

tparts = time.localtime()
fmt = ("%10s: %4d or %d")
print(fmt % ("Year", tparts[0], tparts.tm_year))
print(fmt % ("Month", tparts[1], tparts.tm_mon))
print(fmt % ("Day", tparts[2], tparts.tm_mday))
