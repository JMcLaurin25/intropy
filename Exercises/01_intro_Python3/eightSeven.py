"""
Write a program that counts the number of lines, words, and characters in each file named on the command line.

python3 eightSeven.py file1.txt
"""
#!/usr/bin/env python3
import sys

def main():
	arguments = (sys.argv)
	counts = processArgs(arguments)
	fmt = ("%25s: %s")

	print(fmt %("Number of Lines", counts[0]))
	print(fmt %("Number of words", counts[1]))
	print(fmt %("Number of characters", counts[2]))

def processArgs(args):
	"""Processes multiple arguments to return Line, word, and character counts. (lineCt, wordCt, charCt)"""
	for arg in range(len(args)- 1):
		lineCt = len(countLines(args[arg+1]))
		wordCt = len(countWords(args[arg+1]))
		charCt = countChars(args[arg+1])
	return(lineCt, wordCt, charCt)


def countLines(file):
	data = open(file, 'r')
	lines = data.readlines()
	data.close()
	for item in lines:
		indx = lines.index(item)
		item = item[:-1]
		lines[indx] = item
	return lines

def countWords(file):
	wordList = []
	lines = countLines(file)
	for line in lines:
		words = line.split(" ")
		for word in words:
			wordList.append(word)
	return wordList

def countChars(file):
	charCt = 0
	words = countWords(file)
	for word in words:
		#print(charCt, len(word))
		charCt += len(word)
	return charCt

if __name__=="__main__":
	main()
