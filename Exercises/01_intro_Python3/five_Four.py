"""
Re-write your solution to Exercise 3 using a 'lambda' function.
"""
def outer(int1, int2):
	a = int1
	b = int2

	return lambda : a + b
	#def inner():
	#	x = a
	#	y = b
	#	return x + y
	#return inner

total = outer(3, 2)
print(total())
