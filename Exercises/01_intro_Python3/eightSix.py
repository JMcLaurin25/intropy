"""
Now, create a few more files with one name per line.
	-The program in this exercise should read all these files and print the number of tiems each line occurs over all of the files.
	-The file names should be supplied on the command line.

python3 eightSix.py file1.txt file2.txt file3.txt file4.txt
"""

#!/usr/bin/env python3
import sys

masterList = []
fileList = []

for num in range(len(sys.argv) - 1):
	file = open(sys.argv[num+1], 'r')
	fileList.append(file)

def addtoList(file):
	for line in file:
		masterList.append(line)
	file.close()

for file in fileList:
	addtoList(file)

for item in masterList:
	indx = masterList.index(item)
	if item =="\n":
		masterList.pop(indx)
	else:
		item = item[:-1]
		masterList[indx] = item

masterList.sort() 
masterSet = set(masterList)
print(masterList)
fmt = ("%8s: %d")
for name in masterSet:
	count = masterList.count(name)
	print(fmt %(name, count))

