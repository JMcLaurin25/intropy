"""
Write a program that sums the command line arguments.
	The program should print both the sum of the arguments and the average value.
"""
#!/usr/bin/env python3
import sys

def sortThis(lis):
	lis.sort()
	return lis

def sumThis(data):
	total = 0
	for i in data:
		total += int(i)
	return total

def aveNum(data):
	average = sumThis(data) / len(data)
	return average

argLis = sys.argv[1:]
fmt = ("%25s: %s")
print(fmt % ("Arguments", argLis))
print(fmt % ("Sorted arguments:", sortThis(argLis)))
print(fmt % ("Sum of arguments:", sumThis(argLis)))
print(fmt % ("Average of arguments:", aveNum(argLis)))
