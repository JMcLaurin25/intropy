#!/usr/bin/env python3
def add():
	val = input("Enter value to add:")
	lis.append(val)

def delete():
	x = lis.pop(0)
	print("Removing:", x)

def display():
	print("Displaying:", lis)

def terminate():
	print("Terminating.")
	exit()

lis = []
themap = {1:add, 2:delete, 3:display, 4:terminate}
fmt = ("%2d) %s")
while True:
	for index, fun in themap.items():
		print(fmt % (index, fun.__name__))
	key = int(input("Make selection:"))
	if key in themap.keys():
		themap[key]()
	else:
		print("Illegal Selection\n")	
