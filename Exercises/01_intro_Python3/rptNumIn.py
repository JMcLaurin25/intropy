#!/usr/bin/env python3

numSet = set()
numFailed = 0
print("Submit 'end' to finish.")
while True:
	numIn = input("Enter number:")

	if numIn.lower() == "end":
		break
	else: 
		if numIn in numSet:
			numFailed += 1
		else:	
			numSet.add(numIn)

print(numSet)
print(numFailed, "not added.")
