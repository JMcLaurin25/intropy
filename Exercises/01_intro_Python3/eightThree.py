"""
Add exception handling to Exercise 2 so that if a file open fails, an OSError is handled and the program is halted.
"""
#!/usr/bin/env python3
import sys

try:
	fIn = open(sys.argv[1], 'r')
	fOut = open(sys.argv[2], 'w')

	while True:
		line = fIn.readline()
		if not line:
			break
		fOut.write(line)

	fIn.close()
	fOut.close()
	print("File copy complete.")
except OSError as err:
	print(err)



