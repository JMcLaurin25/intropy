#!/usr/bin/env python3
a = set('efgy')
b = set('exyz')

print("Set a:", a)
print("set b:", b)
print()
print("a | b:", a | b) # union
print("a & b:", a & b) # and/intersection
print("a - b:", a - b) # difference
print("b - a:", b - a) # difference
print("a ^ b:", a ^ b) # xor/symmetric difference
