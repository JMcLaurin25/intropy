"""
Write a program that asks the user for the names of an input and an output file.
	Open both of these files and then have the program read from the input file (use readline) and write to the output file (use write).
	- In effect, this is a copy program.
	- the interface to the program might look like:
		Enter the name of the input file: myinput
		Enter the name of the output file: myoutput
"""
#!/usr/bin/env python3
filename1 = input("Enter filename to copy: ")
filename2 = input("Enter filename of newfile: ")

fIn = open(filename1, "r")
fOut = open(filename2, "w")

while True:
	line = fIn.readline()
	if not line:
		break
	fOut.write(line)

fIn.close()
fOut.close()
print("Copy complete.")
