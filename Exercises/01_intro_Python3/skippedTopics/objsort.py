#!/usr/bin/env python3
class Car:
	def __init__(self, make,  model, year):
		self.make = make
		self.model = model
		self.year = year
	def __str__(self):
		return self.make + " " + self.model + " " + str(self.year)

c1 = Car("Chevy", "Corvette", 1957)
c2 = Car("Mazda", "Miata", 1943)
c3 = Car("Ford", "Mustang", 1950)
c4 = Car("Nissan", "Ultra", 1943)
c5 = Car("Toyota", "Celica", 1962)
data = [c1, c2, c3, c4, c5]
print("MAKE")
data.sort(key=lambda x: x.make)
for c in data:
	print(c)
print("MODEL")
data.sort(key=lambda x: x.model)
for c in data:
	print(c)
print("MAKE")
data.sort(key=lambda x: x.year)
for c in data:
	print(c)
