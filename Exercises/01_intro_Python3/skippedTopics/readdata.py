#!/usr/bin/env python3
from sys import *
def bycityandstate(key):
	return h[key][1] + h[key][2]
def bystate(key):
	return h[key][2]
def bycity(key):
	return h[key][1]
def byage(key):
	return h[key][0]
h = {}
if len(argv) != 2:
	print("Error")
	exit(1)
file = open(argv[1], "r")
line = file.readline()
i = 1
while line:
	name, age, city, state = line.split() 
	h[name] = [int(age), city, state]
	line = file.readline()
	i += 1
print("READ", i - 1, "lines in all.");
print("PRINT SORTED BY NAME");
keys = list(h.keys())
keys.sort()
for key in keys:
	print(key, h[key])

print("PRINT SORTED BY CITY");
keys = list(h.keys())
keys.sort(key=bycity)
for key in keys:
	print(key, h[key])
print()
print("PRINT SORTED BY AGE");
keys = list(h.keys())
keys.sort(key=byage)
for key in keys:
	print(key, h[key])
print()
print("PRINT SORTED BY STATE");
keys = list(h.keys())
keys.sort(key=bystate)
for key in keys:
	print(key, h[key])
print()
print("PRINT SORTED BY CITY AND STATE");
keys = list(h.keys())
keys.sort(key=bycityandstate)
for key in keys:
	print(key, h[key])
