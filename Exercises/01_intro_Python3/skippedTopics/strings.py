#!/usr/bin/env python3
def mylen(p):
	return len(p)
s = ['hi', 'there','Folks', 'folks', 'this', 'is', 'it', 'It', 'This']
print("ORIGINAL STRINGS:")
print(s)
print()

print("Now SORT by length:")
s.sort(key=mylen)
print(s)

print()
s.sort(key=str.lower)
print("Now SORT by case:")
print(s)

print()
