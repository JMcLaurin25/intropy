"""
Write a function that returns a function that, when executed, returns the sum of two integers.
"""

def outer(int1, int2):
	a = int1
	b = int2

	def inner():
		x = a
		y = b
		return x + y
	return inner

total = outer(1, 2)
print(total())
