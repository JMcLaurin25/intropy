num1 = int(input("Enter first number:"))
num2 = int(input("Enter second number:"))

total = 0
numStr = ""
numList = []
while(num1 <= num2):
	total += num1
	numStr += ("%2d + ") % num1
	numList.append(str(("%2d") % num1))
	num1 += 1

print(" + ".join(numList), " = ", total)
print(numStr[:-2], "= ", total)

