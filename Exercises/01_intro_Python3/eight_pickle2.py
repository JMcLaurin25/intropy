#!/usr/bin/env python3
import pickle
f = open("output", "rb")# Read binary

value = pickle.load(f) 
cost = pickle.load(f)
name = pickle.load(f)
f.close()
"""
It looks as though pickle pops off the first value for the first variable requesting it. Then, subsequently pops the next one for the next requestor.
"""

print(name, cost, value)
