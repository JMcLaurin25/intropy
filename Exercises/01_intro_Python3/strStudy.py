#!/usr/bin/env python3

userIn = input("Enter string: ")

lastCh = userIn[len(userIn) - 1]
alphaCh = userIn.isalpha()
findX = userIn.find("x")
length = len(userIn)

if lastCh == ".":
	print("There is a period.")
else:
	print("There is NOT a period.")

print("Is alphabetic?", alphaCh)

if findX == -1:
	print("There is no X")
else:
	print("There is an X")

print("The length is:", length)

finalOut = userIn.replace("e", "E")

print(finalOut)
