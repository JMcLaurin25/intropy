#!/usr/bin/env python3
import six_reusable

a = six_reusable.square(5)
b = six_reusable.cube(10)
print(a, b)
