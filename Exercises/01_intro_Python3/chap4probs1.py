"""
1) Take a list like the one shown here (lol).  

	lol = [ [ 1,2,3], [4,5,6], [7,8], 10, 11, {'a','b'}, {'mike': 37} ]

	It contains potentially:
		a) lists
		b) sets
		c) dicts
		d) single elements

	Write a program which takes that list and flattens it out into
	a list like the one shown here:

	newlist = [ 1,2,3,4,5,6,7,8,10,11,'a','b','mike',37 ]
"""

#!/usr/bin/env python3
dataList = [ [ 1,2,3], [4,5,6], [7,8], 10, 11, {'a','b'}, {'mike': 37} ]

finalList = []
def lsSetFlat(lis):
	for i in lis:
		finalList.append(i)

def dictIn(dic):
	for i in dic:
		finalList.append(i)
		finalList.append(dic[i])

indx = 0	
while indx < len(dataList):
	#print(dataList[indx])
	
	if isinstance(dataList[indx], list) or isinstance(dataList[indx], set):
		lsSetFlat(dataList[indx])
	elif isinstance(dataList[indx], int) or isinstance(dataList[indx], str):
		finalList.append(dataList[indx])
	elif isinstance(dataList[indx], dict):
		dictIn(dataList[indx])
	else:
		print(dataList[indx])

	indx += 1

print(dataList)
print(finalList)
