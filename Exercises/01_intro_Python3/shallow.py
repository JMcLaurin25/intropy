#!/usr/bin/env python3
a = [1,2,3]
b = a
print(id(a))
print(id(b))
print()

a.append(10)
print(a)
print(b)
