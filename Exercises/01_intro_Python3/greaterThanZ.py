#!/usr/bin/env python3

def main():

	numbers = input("Enter numbers (separate with \',\'):")
	numList = numbers.split(",")
	positiveList = []	

	for value in numList:
		num = int(value)
		if num > 0:
			positiveList.append(num)

	positiveList.sort()
	print(positiveList)

	numString = ""
	for i in positiveList:
		numString += str(i) + ', '
	finalOut = numString[0:-2]

	print("(",finalOut,")", "are greater than Zero.")

if __name__ == "__main__":
	main()
