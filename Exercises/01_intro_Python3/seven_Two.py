"""
Test Exercise 1 again by using a few negative numbers as the index.
	Eliminate negative numbers as a legitimate subscripts by raising the 'IndexError'exception when a negative number is given.
"""
#!/usr/bin/env python3
while True:
	numList = [32,25,83,24,67,36,97,18,90,10]
	userIn = input("Enter a number, (end) to quit:")
	if userIn.lower() == "end":
		break
	try:
		data = int(userIn)
		if int(data) < 0:
			raise IndexError()
		print("The number at", data, "is: ", numList[data])
	except ValueError:
		print("Value Error: non numeric data")
	except IndexError:
		print("Index Error: number not within existing list")

