#!/usr/bin/env python3
def square(var):
	return var * var
def cube(var):
	return var * var * var

if __name__=="__main__":
	print("Testing my functions at top level.")
	print(square(5))
	print(cube(10))
