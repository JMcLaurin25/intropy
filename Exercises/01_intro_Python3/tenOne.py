"""
Write a program that reads a line at a time and determines whether the input consists solely of an integer number that is positive or negative.
	Specify whether it is positive or negative.
"""
#!/usr/bin/en python3

import re
while True:
	line = input("Enter a number: ")
	if line.lower() == 'q':
		break	
	x = re.match('^\-?\d*$', line)
	if x:
		if int(x.group(0)) > 0:
			print("Match is positive", line)
		elif int(x.group(0)) < 0:
			print("Match is negative", line)
	else:
		print("Invalid entry >>", line)


