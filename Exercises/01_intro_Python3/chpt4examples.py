"""
Write a code that takes a list of list. Create one list that consists of this elements.
example:[['hello', 'there'], ['hi', 'folks'], ['how', 'are', 'you']]
"""

begLis = [['hello', 'there'], ['hi', 'folks'], ['how', 'are', 'you']]
fullLis = []

for i in begLis:
	for word in i:
		fullLis.append(word)

print(fullLis)
