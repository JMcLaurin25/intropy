"""
Rewrite Exercise 1, but this time get the file names from the command line.
	The interface would look as shown below.
	- Make sure the correct number of command line arguments is provided
	- Otherwise, print an error message and terminate the program.

	python3 program_name inputfile outputfile
"""
#!/usr/bin/env python3
import sys

fIn = open(sys.argv[1], 'r')
fOut = open(sys.argv[2], 'w')

while True:
	line = fIn.readline()
	if not line:
		break
	fOut.write(line)

fIn.close()
fOut.close()

print("File copy complete.")
