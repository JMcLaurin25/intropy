#!/usr/bin/env python3
names = ['Mike', 'John', 'Jane', 'Alice']
theMap = {'Mike':15, 'Chris':10, 'Dave':25}

while True:
	try:
		value = input("\n\nEnter an Integer:")
		if value.lower() == "end":
			break

		value = int(value)
		print("Name is: " + names[value])
		name = input("Enter a name: ")
		print(name, " => ", theMap[name])

	except ValueError:
		print("Value Error: non numeric data")

	except IndexError as ie: # ie is the systems error output
		print("Illegal subscript:", ie)

	except KeyError as ke: # ke is the systems error output, or poor entry
		print("Illegal Key:", ke)

	except:
		print("Unknown Error: ")
