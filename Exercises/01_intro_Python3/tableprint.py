#!/usr/bin/env python3

def main():
	lineStr = ""
	listStr = []
	for i in range(0,50):
		if i % 10 == 0:
			listStr.append(lineStr)
			lineStr = ""
			lineStr += "%5s" % str(i)
		else:
			lineStr += "%5s" % str(i)
		if i == 49:
			listStr.append(lineStr)
	for line in listStr:
		print(line)


if __name__=="__main__":
	main()
