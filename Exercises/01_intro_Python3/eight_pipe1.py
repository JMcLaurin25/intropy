#!/usr/bin/env python3
from subprocess import Popen, PIPE # Imports 'Popen' and 'PIPE' only, from 'subprocess'

p = Popen("ls", stdout=PIPE, universal_newlines=True) #universal_newlines opens files as txt
for line in p.stdout:
	print(line, end="")
p.stdout.close()
return_code = p.wait()
print("Process terminated with return code: ", return_code)

