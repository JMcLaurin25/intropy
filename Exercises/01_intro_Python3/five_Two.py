"""
Write a function that is passed a variable number of arguments and a number, say 'num', as its two parameters.
	The function should return the count of the values in the tuple (the variable number of parameters) that are greater than 'num'.
	- For example, one such call to the function is show below.
		res = pos(5, -10, 10, -20, 30, num=0)
	- In this case, the function would return '3'.
"""

#!/usr/bin/env python3

def varFunc(*args, num):
	count = 0
	for i in args:

		if i > num:
			count += 1
	return count


numPos = varFunc(5, -10, 10, -20, 30, 3, 1, num = 0)

print("\tThere are", numPos, "numbers greater than zero.")
