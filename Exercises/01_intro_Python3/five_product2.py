#!/usr/bin/env python3

def product(*items,begin):
	"""Variable arg passing, with tuple first, args second"""
	res = begin
	for i in items:
		res *= i
	return res

result = product(3, 4, 2, begin = 5)# With tuple first, the arg is explicitly declared. (begin = 5)
print(result)
