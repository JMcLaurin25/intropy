#!/usr/bin/env python3
import math

fmt = ("%25s: % 10.4f")
fmtD = ("%25s: % 7.1f")

print(fmt % ("Square Root of 10", math.sqrt(10)))
print(fmt % ("64 to 3/2 pow", math.pow(64,1.5)))
print(fmt % ("Hypotenuse of 6 and 8", math.hypot(6,8)))
radians = math.radians(360)
print(fmt % ("Radians to Degrees", radians))
print(fmt % ("Degrees to Radians", math.degrees(radians)))

print(fmtD % ("Round 2.5 up", float(math.ceil(2.5))))
print(fmtD % ("Round 2.5 down", float(math.floor(2.5))))

print(fmt % ("Pi", math.pi))
