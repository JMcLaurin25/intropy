"""
Create two data files, each with a set of names, one per line.
	Now, write a program that reads both files and lists only those names that are in both files.
	The two file names should be supplied on the command line.
"""
#!/usr/bin/env python3
import sys

file1 = open(sys.argv[1], 'r')

for line1 in file1:
	file2 = open(sys.argv[2], 'r')
	for line2 in file2:
		if line1 == line2:
			print(line1, end="")
	file2.close()

file1.close()

