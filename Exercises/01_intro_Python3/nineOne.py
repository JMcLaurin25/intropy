"""
Create a class called Person
	Each 'Person' should have a 'name', and 'age', and a 'gender'.

	In addition to getters and setters for the above methods, the 'Person' class should have a '__init__' method and a '__str__' method.

	Therefore, the following application should test your work.
	- p1 = Person("Michael", 45, "M")
	- print(p1)
"""
#!/usr/bin/env python3
class Person:
	def __init__(self, name, age, gender):
		self.myname = name
		self.myage = age
		self.mygender = gender
	#--Getters
	def name(self):
		return self.myname
	def age(self):
		return self.myage
	def gender(self):
		return self.mygender
	#--Setters
	def setname(self, newname):
		self.myname = newname
	def setage(self, newage):
		self.myage = newage
	def setgender(self, newgender):
		self.mygender = newgender

	def __str__(self):
		ageStr = str(self.myage)
		return self.myname + ", " + ageStr + ", " + self.mygender
