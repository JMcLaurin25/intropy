#!/usr/bin/env python3
bitvar = 0o755 # octal
print(bin(bitvar))

bitno = int(input("Enter a bit number:")
print("You entered bit number: ", bitno)

mask = 1 << (bitno - 1) # Creates a mask that puts 1 in the 'bitno' place

print(bin(mask))

# '&' checks the original number against the mask to verify if it is 'on'
if mask & bitvar: # if '&' is true
	print("Bit number", bitno, "is set")
else:	# if '&' is false
	print("Bit number", bitno, "is NOT set")

