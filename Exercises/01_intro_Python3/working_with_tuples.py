#!/usr/bin/env python3

x = 10;
y = 20;
data = (x, y, 5, 30, 7) # Tuple
print("Tuple contents:", data)
print("A slice:", data[2:4])

#Convert tuple to list
as_list = list(data)
as_list.sort()
print("List:", as_list)

#Convert list to tuple
data = tuple(as_list)
print("Tuple contents:", data)
