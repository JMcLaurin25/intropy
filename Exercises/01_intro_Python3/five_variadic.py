#!/usr/bin/env python3

def theSum(*args):
	"""Sums the args stuff"""
	total = 0
	print(type(args))
	for elem in args:
		total += elem
	return total

x = theSum(1,2,3,4,5)
print(x)
x = theSum(1,2,3)
print(x)
