"""
Write and test a function that takes a number and a dictionary and adds the number to all values in the dictionary.
	- You can assume that all the values (but not necessarily the keys) in the dictionary are numbers.
"""

def addDict(num, **items):
	for person in items.keys():
		items[person] += num
	return items

output = addDict(2, jay=100, alex=200, dave=300, bryan=400)
print(output)
