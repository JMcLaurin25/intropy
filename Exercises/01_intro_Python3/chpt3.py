#!/usr/bin/env python3

"""
a) Write a Python program that receives a line of input from the user. Split the line on white space. Now you should have a bunch of words in a list.

Write a loop that iterates over those words and sums all of the owrds that represent integers.
[Hello 123 and how are 456 and 37]

b) Place a loop around (a) so that you can input other lines as well.

c) Provide a way of having the user exit the program.

d) When the user chooses to exit, ask the user if they want a total of all the lines.
"""

def addWdNum(wdLst):
	numTotal = 0	
	for word in words:
		dig = word.isdigit()
		print(dig)
		if dig == True:
			num = int(word)
			numTotal += num
	return numTotal

finalTotal = 0
while True:
	print("If no more entries, enter 'exit'")
	dataIn = input("Enter sentence:")
	words = dataIn.split()
	curSum = addWdNum(words)
	finalTotal += curSum
	if dataIn.lower() == "exit":
		break
	print("Sum:", curSum)

print()
finalPrint = input("Would you like the final sum? (y/n):")
if finalPrint.lower() == "y":
	print("This is the final total of the numbers:", finalTotal, "\n")
else:
	print("Goodbye\n")
