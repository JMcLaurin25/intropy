#!/usr/bin/env python3
import time

now = time.time() # Time as seconds since epoch
print("Seconds since epoch (1/1/1970):", now)

date = time.ctime(now) # Time as the date
print("Date as a string is:", date)

pieces = time.localtime() # Grab pieces of time
print()
print(pieces)
print()
print(type(pieces))
for piece in pieces: # print each piece
	print(piece)

print(time.strftime("YR/MO/DY is %Y/%m/%d"))
