#!/usr/bin/env python3

radIn = input("Please enter radius: ")

radius = float(radIn)
pi = 3.14159
area = pi * (radius * radius)

print("Your circle of ", radius, "has an area of", area)
