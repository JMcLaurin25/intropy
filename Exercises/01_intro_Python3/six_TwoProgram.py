"""
Create a new file and define a function in it with the same name as one of the functions form the previous exercise.
	Within the same file, create an application that imports module from the previous exercise (six_OneModule.py).
	- The application should call both the function from the imported module, and the function within the current module.
"""
#!/usr/bin/env python3
import six_OneModule

def forwardOut(var):
	sentence = "This is the ordinary output: "+ var
	return sentence

data = input("Please enter word:")

print(six_OneModule.forwardOut(data))
print(forwardOut(data))
