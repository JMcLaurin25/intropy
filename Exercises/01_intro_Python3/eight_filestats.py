#!/usr/bin/env python3
import sys, os, time

tag = ["mode", "inode#", "device#", "#links", "user", "group", "bytes", "last access", "last modified", "change/creation time"]
fmt = ("%25s: %s")

def printstats01(file, stat):
	print("File Stats for:", file)
	print(fmt % (tag[0], oct(stat.st_mode)))
	print(fmt % (tag[1], stat.st_ino))
	print(fmt % (tag[2], stat.st_dev))
	print(fmt % (tag[3], stat.st_nlink))
	print(fmt % (tag[4], stat.st_uid))
	print(fmt % (tag[5], stat.st_gid))
	print(fmt % (tag[6], stat.st_size))
	print(fmt % (tag[7], time.ctime(stat.st_atime)))
	print(fmt % (tag[8], time.ctime(stat.st_mtime)))
	print(fmt % (tag[9], time.ctime(stat.st_ctime)))
	print("\n")

def printstats02(file, stats):
	print("file Stats for:", file)
	for i, a_stat in enumerate(stats): #stats is a tuple
		print(fmt % (tag[i], a_stat))
	print("\n")

for file in sys.argv[1:]:
	info = os.stat(file)
	if os.path.isfile(file):
		print("*" * 30)
		printstats01(file, info)
		printstats02(file, info)
