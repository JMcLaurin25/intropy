"""
In Python, if you wish to reverse sort a list, you will need to do the following.

	values = [10, 40, 30, 20, 5]
	values.sort()
	values.reverse()
	print(values)

	- Write your own versions of 'sort' and 'reverse' so that each of the following is possible
		values = [10,40,30,20,5]
		print(sort(values))
		print(reverse(values))
		print(reverse(sort(values))
		
		* Make sure the lists themselves are not altered.
"""

originalValues = [10,30,40,20,5]
cpVals = set(originalValues)
fmt = ("%20s: %s")

def sort(valIn):
	values = list(valIn)	
	for i in range(len(values)):
		for j in range(len(values)):
			if values[i] == values[j]:
				continue
			elif values[i] < values[j]:
				temp = values[i]
				values[i] = values[j]
				values[j] = temp
	return values

def reverse(valIn):
	return valIn[::-1]

print(fmt %("Values", str(originalValues)))
print(fmt %("Sorted", str(sort(originalValues))))
print(fmt %("Reversed", str(reverse(originalValues))))
print(fmt %("Reversed/Sorted", str(reverse(sort(originalValues)))))
