"""
Write a program that displays the file name, size, and modification date for all those files in a directory that are greater than a certain size.
	:The directory name and the size criteria are given as command line arguments.
	:If the number of command line arguments is incorrect, the program should print an error message and terminate.
"""
import sys, os, time

fmt = ("%20s: %s")

if len(sys.argv) != 2:
	print("Wrong Number of arguments:", sys.argv, " (2) Needed.")
	exit(1)

def filedata(file, stats):
	print(fmt % ("File name", file))
	print(fmt % ("File size", stats.st_size))
	print(fmt % ("Modification date", stats.st_mtime))

for file in os.listdir():
	info = os.stat(file)
	if info.st_size < int(sys.argv[1]):
		#print("in size comparo")
		#print name, size, date
		filedata(file, info)
		print()


