#!/usr/bin/env python3
from subprocess import Popen, PIPE
p = Popen(["ls", "-la"], stdout=PIPE, universal_newlines=True)
#Note that the command given is split up. ["ls", "-la"]

for line in p.stdout:
	print(line, end="")# Overwrite auto \n. Popen, automatically reads the \n.
p.stdout.close()
return_code = p.wait()
print("Process terminated with return codes: ", return_code)
