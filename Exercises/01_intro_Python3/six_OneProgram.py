"""
Create a few functions and place them in a file.
	Now, write a Python program that imports the module.
	- Use the functions from the module in this program.
"""
#!/usr/bin/env python3
import six_OneModule

while True:
	wordIn = input("Enter a word, 'q' to quit: ")
	if wordIn.lower() == 'q' or wordIn.lower() == 'quit':
		break

	makeFor = six_OneModule.forwardOut(wordIn)
	makeRev = six_OneModule.reverseOut(wordIn)
	numIn = input("Enter a num: ")
	mult = six_OneModule.multOut(wordIn, numIn)

	print(makeFor)
	print(makeRev)
	print(mult)	
