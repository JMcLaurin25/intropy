"""
2) 	Write a program that counts the frequencies of all lower case 
	characters in the user's input.  As usual, input the data as 
	lines from the keyboard.  Use quit as we have been doing to end
	the input.
"""

#!/usr/bin/env python3
lowCount = 0
while True:
	userIn = input("Enter word: (type 'exit' to exit)")

	if userIn.lower() == 'exit':
		break
	for ch in userIn:
		if ch.islower():
			lowCount += 1

print("Number of lower case words:", lowCount)
