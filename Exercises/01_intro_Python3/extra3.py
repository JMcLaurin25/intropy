#!/usr/bin/env python3
"""
See if you can redo problem (2) by inputting all three values on One line
"""
print("Enter number of rows, columns, and word:")
entry = input("(4, 5, banana) >>")
print(entry)

entryList = entry.split(',')

value1 = int(entryList[0])
value2 = int(entryList[1])
value3 = entryList[2].lstrip()

for i in range(value2):
	print((value3 + " ") * value1)
