"""
Write and test a function that takes two lists as arguments and returns a list of the elements that are common to both of the argument lists.
"""
#!/usr/bin/env python3

def joinLike(args1, args2):
	matches = []	
	for val1 in args1:
		for val2 in args2:
			if val1 == val2:
				matches.append(val1)
	return matches

print(joinLike([1,2,3,4,5], [6,2,3,1,5]))
