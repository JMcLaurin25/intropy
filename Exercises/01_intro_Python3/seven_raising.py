#!/usr/bin/env python3

while True:
	result = input("Enter an even number:")
	if result.lower() == "end":
		break
	try:
		pos = int(result)
		if int(pos) % 2 != 0:
			raise ValueError("# Not Even:" + str(pos))
		print("Number is", pos)
	except ValueError as err:
		print(err)
