"""
Create an exception class named 'FamilyError'
	This exception should get raised when there is an attempt to add a non-'Person' object to a 'Family' object.
	- Remember that 'Person' objects can be added in both the __init__ method and the 'add' method.
"""
#!/usr/bin/env python3
class FamilyError(Exception):
	def __init__(self, msg, person):
		Exception.__init__(self, msg)
		self.person = person

	def person(self):
		return self.person
