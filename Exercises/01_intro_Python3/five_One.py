"""
Write a function that receives a list as its parameter and returns a new list of the positive elements only.
"""
#!/usr/bin/env python3

def isPos(lis):
	outList = []
	for num in lis:
		if int(num) > 0:
			outList.append(num)
	return outList

dataLis = []
while True:
	dataIn = input("Enter ',' seperated numbers, (q) to quit:")
	if dataIn.lower() == "q":
		break
	dataSpl = dataIn.split(",")
	
	for i in dataSpl:
		dataLis.append(i)	
	print(dataLis)

posList = isPos(dataLis)

print("These are the positive elements:", posList)
print()
