#!/usr/bin/env python3

def main():
	lineList = []
	listStr = []
	for i in range(0,50):
		if i % 10 == 0:
			if i != 0:
				listStr.append(lineList)
				lineList = []
				lineList.append(str(i))
		lineList.append(str(i))
		if i == 49:
			listStr.append(lineList)
	for line in listStr:
		print(line)


if __name__=="__main__":
	main()
