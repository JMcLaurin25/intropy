#!/usr/bin/env python3
var = 0o177777;
str = "bits are: {0:016b}"
print("TO BEGIN: ", str.format(var))
while True:
	n = int(input("Enter a bit number to turn off: "))
	#
	#	set up mask
	#
	mask = 1 << (n - 1)
	str = "bits are: {0:016b}"
	print("LOOP: ", str.format(mask))
	var = var ^ mask
	print(bin(var))
"""
	var =  	1 111 111 111 111 111
	mask = 	0 000 000 001 000 000   for n = 7
		=====================
	var ^ n 1 111 111 110 111 111
"""
