#!/usr/bin/env python3
import random

def code():
    """ Generates a new random 4 digit code. Returns string"""
    codeStr = ""
    codeStr += (str(random.randint(1,9)))
    while len(codeStr) < 4:
        newDigit = random.randint(1,9)
        if codeStr.find(str(newDigit)) == -1:
            codeStr += (str(newDigit))
    return codeStr

def compare(master, guess):
    """ Compares the master code with the users guess. Returns tuple (int, int) of red/white results."""
    red = 0
    white = 0
    position = 0    

    for position in range(4):
        if master[position] == guess[position]:
            red += 1

    for value in range(len(master)):# returns whites(correct number)
        #print(master[value])
        for valueGuess in range(len(guess)):
            if guess[valueGuess] == master[value]:
                white += 1
            #print(guess[valueGuess])

    return(red, (white - red))

def main():
    """Main function for running the Mastermind-Game"""
    masterCode = code()
    print("> MASTERMIND")
    fmt = ("%8s: %s")
    print(("-" * 20) + "Key" + ("-" * 20))
    print(fmt % ("red", "correct number and position."))
    print(fmt % ("white", "correct number wrong position."))
    print(("-" * 43))
    #print(masterCode)
    attempt = 0

    while True:
        userGuess = input("Guess a number:")
        if userGuess.lower() == 'q':
            break
        if not userGuess.isdigit() or len(userGuess) != 4:
            print("Please provide a 4 digit guess.")
        else:
            attempt += 1
            results = compare(masterCode, userGuess)
            if results[0] == 0:
                print(results[1], "white")
            elif results[1] == 0:
                print(results[0], "red")
            else:
                print(results[0], "red,", results[1], "white")
            if userGuess == masterCode:
                if attempt == 1: # Corrects plurality of the final word "guess(es)"
                    isplural = "guess."
                else:
                    isplural = "guesses."
                print("You win! It took you", attempt, isplural)
                break
    playAgain = input("Play again, y/n? ")
    if playAgain.lower() == "y" or playAgain.lower() == "yes":
        main()
    else:
        pass

if __name__=="__main__":
    main()
