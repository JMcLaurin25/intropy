#!/usr/bin/usr python3
import sys

def convert(numBtl):
    """Converts the numerical number into letters. Returns a string value."""

    onesDic = {0:"none", 1:"One", 2:"Two", 3:"Three", 4:"Four", 5:"Five", 6:"Six", 7:"Seven", 8:"Eight", 9:"Nine"}
    teenDic = {10:"Ten", 11:"Eleven", 12:"Twelve", 13:"Thirteen", 14:"Fourteen", 15:"Fifteen", 16:"Sixteen", 17:"Seventeen", 18:"Eighteen", 19:"Nineteen"}
    tensDic = {2:"Twenty", 3:"Thirty", 4:"Forty", 5:"Fifty", 6:"Sixty", 7:"Seventy", 8:"Eighty", 9:"Ninety"}

    strBt = str(numBtl)
    if len(strBt) == 2:
        tens = int(strBt[0])
        ones = int(strBt[1])
        if tens == 1: # Between 10 - 19
            wdNum = teenDic[int(strBt)]
        else:
            btTens = tensDic[tens]
            btOnes = onesDic[ones]
            if btOnes == "none":
                wdNum = btTens
            else:
                wdNum = btTens + "-" + btOnes
    else: #Convert for single digit number
        wdNum = onesDic[int(strBt)]
    return wdNum

def fmtVerse(numBottle, numStyle):
    """Generates each verse of the song. Returns a string value."""
    verse = ""
    if numBottle == 1:
        fmtBottle = "bottle"
        numlastBottleOut = "No more"
        fmtFinalBottle = "bottles"
    else:
        fmtBottle = "bottles"
        numlastBottleOut = str(numBottle - 1)
        if (numBottle - 1) == 1:
            fmtFinalBottle = "bottle"
        else:
            fmtFinalBottle = "bottles"

    if numStyle == "no": # Converts number to alphabetic
        bottleOut = convert(numBottle)
        if numlastBottleOut.isdigit():
            lastBottleOut = convert(numlastBottleOut)
        else:
            lastBottleOut = numlastBottleOut
    else: # Retains numeric format
        bottleOut = str(numBottle)
        lastBottleOut = numlastBottleOut
    
    verse = bottleOut + " " + fmtBottle + " of beer on the wall!\n" + bottleOut + " " + fmtBottle + " of beer!\n" + "Take one down\nAnd pass it around\n" + lastBottleOut + " " + fmtFinalBottle + " of beer on the wall!\n"

    return verse

def main():
    """Main function for creating verses of '99 Bottles of beer on the wall'"""

    if len(sys.argv) >= 2 and sys.argv[1].isdigit(): # Outputs numerical verse beginning with argument provided number
        isnumeric = "no" 
        btCnt = int(sys.argv[1])
    elif len(sys.argv) == 2 and sys.argv[1] == '-n': # Outputs 'numerical' verse with default 99 beginning
        print("Will convert to Text")
        isnumeric = "yes"
        btCnt = 99
    elif len(sys.argv) == 3 and sys.argv[1] == '-n': # Outputs 'numerical' verse with argument provided number
        isnumeric = "yes"
        btCnt = int(sys.argv[2])
    else: # Outputs default verse with default beginning '99'
        isnumeric = "no"
        btCnt = 99

    initCt = str(btCnt)
    while btCnt > 0: # Decrements from high number to 0
        print(fmtVerse(btCnt, isnumeric))
        btCnt -= 1

    print("Bottle count started with:", initCt)

if __name__=="__main__":
    main()
