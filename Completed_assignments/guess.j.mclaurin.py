import random, time #Random creates the mystery number. Time is used to determine when to lie.

def tolie():
    """ Function to Lie if the first Ones digit is divisible with 2 """
    num = int(str(time.time())[-1])
    if num % 5 == 0:
        toLie = 'yes'
    else:
        toLie = 'no'
    #print(toLie, num)
    return toLie

def startGame():
    """This function prints the Welcome"""
    print("> guessing\nI'm thinking of a number between 1 to 100")
    return random.randint(1,100) # Generates the random number

def main():
    """Main function for running the Guessing-Game"""
    gameDone = False # Game loop condition
    guessAttempt = 0
    lieCount = 0
    lieStore = 0 #Stores the value that was lied about
    hiLow = ""

    pcNum = startGame()#Starts the game. Gets random number to guess
    usrIn = input("Try to guess my number, 'q' to quit:")

    while not gameDone:
        try:
            if usrIn.lower() == 'q': #Allows for quitting the program
                break
            guess = int(usrIn)
            guessAttempt += 1

            if guess == pcNum:
                if guessAttempt == 1: # Corrects plurality of the final word "guess(es)"
                    isplural = " guess"
                else:
                    isplural = " guesses"
                if lieCount == 1:
                    print(str(guess) + " is correct! You guessed my number in " + str(guessAttempt) + isplural + ". I lied about " + str(lieStore) + " being " + hiLow)
                else:
                    print(str(guess) + " is correct! You guessed my number in " + str(guessAttempt) + isplural + ".")
                gameDone = True
            else: # Measure if low/high
                if guess > 100 or guess < 0:
                    usrIn = input(str(guess) + " is not a valid guess - please guess again within the boundaries. ")
                    guessAttempt -= 1
                elif guess > pcNum: # Guess too high
                    if tolie() == 'yes' and lieCount == 0: # When telling a lie
                        usrIn = input(str(guess) + " is too low - please guess again: ")# LIES!!
                        lieCount = 1
                        lieStore = guess
                        hiLow = "too low."
                    else: # Telling the truth
                        usrIn = input(str(guess) + " is too high - please guess again: ")
                elif guess < pcNum: # Guess too low
                    if tolie() == 'yes' and lieCount == 0: # When telling a lie
                        usrIn = input(str(guess) + " is too high - please guess again: ")# LIES!!
                        lieCount = 1
                        lieStore = guess
                        hiLow = "too high."
                    else: # Telling the truth
                        usrIn = input(str(guess) + " is too low - please guess again: ")

        except ValueError:
            print("Value is not a valid integer - Try Again")
            usrIn = input("Try to guess my number:")            

if __name__=="__main__":
    main()
